/********************************************************************************
	
	Name:			DPHHS_DBrief_Email_Reminder.js
	Author:			csj070 (CMurnion)
	Created:		04/02/2020
	Last Updated:	
	For Version:	6.x/7.x
	Script Version: 
---------------------------------------------------------------------------------
	Summary:
		This script will send an email reminder to an assignee who has
		any decision requests in their queue that require action and have
		been inactive for at least 3 days.
		
	Mod Summary:
		
		
	Execution Method:
		This script is designed to be run as workflow within.
		
********************************************************************************/

// ********************* Include additional libraries *******************
// Linked Libraries
//#link "inxml"    //XML parser
//#link "sedbc"    //Database object
#link "secomobj" //COM object

// STL packages
#if defined(imagenowDir6)
	// 6.7.0.2717+, including Active-Active support
	#include "$IMAGENOWDIR6$/script/STL2/packages/Date/dateDiff.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Logging/iScriptDebug.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Logging/StatsObject.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/User/findUsers.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/User/getUserProperties.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Email/iEmail.js"
#else
	// pre-6.7.0.2717, no Active-Active support
	#include "../script/STL2/packages/Date/dateDiff.js"
	#include "../script/STL2/packages/Logging/iScriptDebug.js"
	#include "../script/STL2/packages/Logging/StatsObject.js"
	#include "../script/STL2/packages/User/findUsers.js"
	#include "../script/STL2/packages/User/getUserProperties.js"
	#include "../script/STL2/packages/Email/iEmail.js"
#endif

// *********************         Configuration        *******************

var CONFIG_VERIFIED     = true;   // set to true when configuration values have been verified

// Logging
var LOG_TO_FILE         = true;    // false - log to stdout if ran by intool, true - log to inserverXX/log/ directory
var DEBUG_LEVEL         = 5;       // 0 - 5.  0 least output, 5 most verbose
var SPLIT_LOG_BY_THREAD = false;   // set to true in high volume scripts when multiple worker threads are used (workflow, external message agent, etc)
var MAX_LOG_FILE_SIZE   = 100;     // Maximum size of log file (in MB) before a new one will be created

var DRY_RUN = false;

//E-mail Configuration
var EMAIL_CONFIG = {
	IsHTML		: false,
	useUtility	: false,	// if this is true, ASPEmail will need to be installed on the server running this script.
	useCDO		: false,
	smtp		: "mail.mt.gov",
	from		: "ecmdev@mt.gov",
	to			: "",
	cc			: "",
	bcc			: "", //emailConfig.bodyParams
	emailsubject		: "",
	emailbody		: "",
};

// *********************       End  Configuration     *******************

// ********************* Initialize global variables ********************
var EXECUTION_METHODS = ["WORKFLOW"]; //Allowed script execution methods: WORKFLOW, INTOOL, TASK, EFORM, EMA, INTEGRATION
var debug;

/**
 * Main body of script.
 * @method main
 * @return {Boolean} True on success, false on error.
 */
function main ()
{
	try
	{
		debug = new iScriptDebug("USE SCRIPT FILE NAME", LOG_TO_FILE, DEBUG_LEVEL, undefined, {splitLogByThreadID:SPLIT_LOG_BY_THREAD, maxLogFileSize:MAX_LOG_FILE_SIZE});
		debug.showINowInfo("INFO");
		debug.logAlways("INFO", "Script Version: $Id: SOM_BFSDJournalNotifications.js 40343 2019-04-11 08:54:05Z rhalder $\n");
		debug.logAlways("INFO", "Script Name: %s\n", _argv[0]);
		var stats = new StatsObject();

		if (!CONFIG_VERIFIED)
		{
			var errorStr = "Configuration not verified.  Please verify \n" +
			"the defines in the *** Configuration *** section at the top \n" +
			"of this script and set CONFIG_VERIFIED to true.  Aborting.\n";
			debug.logln("CRITICAL", errorStr);
			printf(errorStr);
			return false;
		}
		
		// check script execution
		if (!debug.checkExecution(EXECUTION_METHODS))
		{
			debug.logln("CRITICAL", "This iScript is running as [%s] and is designed to run from [%s]", debug.getExecutionMethod(), EXECUTION_METHODS);
			return false;
		}
		
		// Get the current workflow item
		var wfItem = new INWfItem(currentWfItem.id);
		if (!wfItem.id || !wfItem.getInfo())
		{
			debug.logln("CRITICAL", "Couldn't get info for workflow item [%s]: %s", currentWfItem.id, getErrMsg());
			return false;
		}
		
		// Get the time (in milliseconds) of how long the workflow item has been in this queue

		var timeInQueue = wfItem.queueStartTime
		var currentDate = new Date();
		
		// If item is more than 3 days (86400000 ms * 3) old, send the email notification.
		if (dateDiff(currentDate, timeInQueue, "days") > 3) {
			// Get the document and custom properties from the workflow item
			wfDoc = new INDocument(wfItem.objectId);
			var docLink = wfDoc.createLink("C:\\")
			
			var props = wfDoc.getCustomProperties();
			
			// Get the current assignee, requester names and process status from custom properties
			// Assignee and requester IDs are hidden fields on the form. If the form is at a stopping point
			// (approved or denied) assigneeID will be "process_completed"
			var assigneeID = "";
			var decisionName = "";
			var assigneeEmail = "";
			
			for (var i = 0; i < props.length; i++) {
				if (props[i].name == "DPHHS - DBrief Current Assignee ID") {
					assigneeID = props[i].getValue();
				} else if (props[i].name == "DPHHS - DBrief Decision Name") {
					decisionName = props[i].getValue();
				} else if (props[i].name == "DPHHS - DBrief Assignee Email") {
					assigneeEmail = props[i].getValue();
				}
			}
			
			if (assigneeID == "") {
				debug.logln("CRITICAL", "Couldn't get current assignee ID from document %s", wfDoc.id);
				return false;
			}
			
			if (decisionName == "") {
				debug.logln("CRITICAL", "Couldn't get decision name from document %s", wfDoc.id);
				return false;
			}
			
			if (assigneeEmail == "") {
				debug.logln("CRITICAL", "Couldn't get assignee email from document %s", wfDoc.id);
				return false;
			}
			
			// Send email to current assignee
			
			if (!sendEmail(assigneeEmail, decisionName, docLink)) {
				debug.logln("CRITICAL", "Failed to send email to %s. Review server logs.", assigneeEmail);
				return false;
			}
		}
	}
	catch(e)
	{
		if (!debug)
		{
			printf("\n\nFATAL iSCRIPT ERROR: %s\n\n", e.toString());
		}
		else
		{
			debug.setIndent(0);
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "**                                           **");
			debug.logln("CRITICAL", "**    ***    Fatal iScript Error!     ***    **");
			debug.logln("CRITICAL", "**                                           **");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "\n\n\n%s\n\n", e.toString());
			debug.logln("CRITICAL", "\n\nThis script has failed in an unexpected way.  Please\ncontact Hyland Software Customer Support at 800-941-7460 ext. 2");
			debug.logln("CRITICAL", "Alternatively, you may wish to email es_support@hyland.com\nPlease attach:\n - This log file");
			debug.logln("CRITICAL", " - The associated script [%s]\n - Any supporting files that might be specific to this script\n", _argv[0]);
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			if (DEBUG_LEVEL < 3 && typeof(debug.getLogHistory) === "function")
			{
				debug.popLogHistory(11);
				debug.logln("CRITICAL", "Log History:\n\n%s\n", debug.getLogHistory());
			}
		}
	}
	finally
	{
		if (debug) debug.setIndent(0);
		if (typeof(stats) == "object" && stats)
		{
			if (stats.isEmpty()) stats.inc("Stats successfully generated.");
			if (debug) debug.logAlways("NOTIFY", "Done:\n\n%s\n", stats.getSortedStats());
			else printf("Done:\n\n%s\n", stats.getSortedStats());
		}
		if (debug) debug.finish();
	}
}	

// ********************* Function Definitions **********************************

/** ****************************************************************************
  * Send email
  *
  * @param {String} email address
  * @param {Array} email subject and body
  * @return {Boolean} true on success,false on error
  *****************************************************************************/
function sendEmail(sendTo, decisionName, docLink)
{
	var strSubject	= "Action Required on Decision Request: " + decisionName
	var strBody		= "Decision request " + decisionName + " requires action from you and has been inactive for more than 3 days. " +
					  "Please log into Perceptive Content and take action on the decision request. \n Document link: " + docLink
	
	var emailConfig = 
	{
		smtp:		EMAIL_CONFIG.smtp,
		to:			sendTo,
		from:		EMAIL_CONFIG.from,
		cc:			EMAIL_CONFIG.cc,
		bcc:		EMAIL_CONFIG.bcc,
		subject:	strSubject,
		body:		strBody,
		IsHTML:		EMAIL_CONFIG.IsHTML,
		useUtility:	EMAIL_CONFIG.useUtility,
	};
	
	var email = new iEmail(sendTo, emailConfig);
	
	//send e-mail
	if(!email.send())
	{
		debug.logln("ERROR", "sendEmail: Error sending email to %s: %s.",sendTo,getErrMsg());
		return false;
	}
	else
	{
		debug.logln("INFO", "sendEmail: Email sent successfully..");
	}
	return true;
}