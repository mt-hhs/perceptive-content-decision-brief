/********************************************************************************
	
	Name:			DPHHS_DBrief_Route_Approved.js
	Author:			csj070 (CMurnion)
	Created:		04/02/2020
	Last Updated:	
	For Version:	6.x/7.x
	Script Version: 
---------------------------------------------------------------------------------
	Summary:
		This script creates copies of an approved decision request and routes
		them to the appropriate workflow queues. It will also set the next assignee
		who must take action on the request next.
		
	Mod Summary:
		
		
	Execution Method:
		This script is designed to be run as workflow within.
		
********************************************************************************/

// ********************* Include additional libraries *******************
// Linked Libraries
//#link "inxml"    //XML parser
//#link "sedbc"    //Database object
#link "secomobj" //COM object

// STL packages
#if defined(imagenowDir6)
	// 6.7.0.2717+, including Active-Active support
	#include "$IMAGENOWDIR6$/script/STL2/packages/Logging/iScriptDebug.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Logging/StatsObject.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Document/copyDocument.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/User/findUsers.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/User/getUserProperties.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Workflow/createOrRouteItem.js"
    #include "$IMAGENOWDIR6$/script/STL2/packages/Workflow/routeItem.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Form/FormManager.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Email/iEmail.js"
#else
	// pre-6.7.0.2717, no Active-Active support
	#include "../script/STL2/packages/Logging/iScriptDebug.js"
	#include "../script/STL2/packages/Logging/StatsObject.js"
	#include "../script/STL2/packages/Document/copyDocument.js"
	#include "../script/STL2/packages/User/findUsers.js"
	#include "../script/STL2/packages/User/getUserProperties.js"
	#include "../script/STL2/packages/Workflow/createOrRouteItem.js"
	#include "../script/STL2/packages/Form/FormManager.js"
	#include "../script/STL2/packages/Email/iEmail.js"
#endif

// *********************         Configuration        *******************

var CONFIG_VERIFIED     = true;   // set to true when configuration values have been verified

// Logging
var LOG_TO_FILE         = true;    // false - log to stdout if ran by intool, true - log to inserverXX/log/ directory
var DEBUG_LEVEL         = 5;       // 0 - 5.  0 least output, 5 most verbose
var SPLIT_LOG_BY_THREAD = false;   // set to true in high volume scripts when multiple worker threads are used (workflow, external message agent, etc)
var MAX_LOG_FILE_SIZE   = 100;     // Maximum size of log file (in MB) before a new one will be created

var DRY_RUN = false;


// *********************       End  Configuration     *******************

// ********************* Initialize global variables ********************
var EXECUTION_METHODS = ["WORKFLOW"]; //Allowed script execution methods: WORKFLOW, INTOOL, TASK, EFORM, EMA, INTEGRATION
var debug;

/**
 * Main body of script.
 * @method main
 * @return {Boolean} True on success, false on error.
 */
function main ()
{
	try
	{
		debug = new iScriptDebug("USE SCRIPT FILE NAME", LOG_TO_FILE, DEBUG_LEVEL, undefined, {splitLogByThreadID:SPLIT_LOG_BY_THREAD, maxLogFileSize:MAX_LOG_FILE_SIZE});
		debug.showINowInfo("INFO");
		debug.logAlways("INFO", "Script Version: $Id: SOM_BFSDJournalNotifications.js 40343 2019-04-11 08:54:05Z rhalder $\n");
		debug.logAlways("INFO", "Script Name: %s\n", _argv[0]);
		var stats = new StatsObject();

		if (!CONFIG_VERIFIED)
		{
			var errorStr = "Configuration not verified.  Please verify \n" +
			"the defines in the *** Configuration *** section at the top \n" +
			"of this script and set CONFIG_VERIFIED to true.  Aborting.\n";
			debug.logln("CRITICAL", errorStr);
			printf(errorStr);
			return false;
		}
		
		// check script execution
		if (!debug.checkExecution(EXECUTION_METHODS))
		{
			debug.logln("CRITICAL", "This iScript is running as [%s] and is designed to run from [%s]", debug.getExecutionMethod(), EXECUTION_METHODS);
			return false;
		}
		
		// Get the current workflow item
		var wfItem = new INWfItem(currentWfItem.id);
		if (!wfItem.id || !wfItem.getInfo())
		{
			debug.logln("CRITICAL", "Couldn't get info for workflow item [%s]: %s", currentWfItem.id, getErrMsg());
			return false;
		}



        if (!routeItem(wfItem, "CSM316 (DPHHS - DBrief Review)", "Routing for approval"))
        {
            debug.log("ERROR", "Failed to route workflow item [%s] to queue [%s]\n", wfItem.id, approvalQueueName);
            return false;
        }


		
		/*// Get the document and custom properties from the workflow item
		wfDoc = new INDocument(wfItem.objectId);
		var props = wfDoc.getCustomProperties();
		
		// Check to see which sub-processes the request needs to go to
		var subprocesses = [];
		for (var p = 0; p < props.length; p++) {
			if (props[i].name == "DPHHS - DBrief Tribal Impact") {
				subprocesses.push("tribal");
			}
		}
		
		
		// Create new document(s) and copy over form
		for (var i = 0; i < subprocesses.length; i++) {
			// Create new document keys
			var keys = new INKeys("Drawer", "Field1", "Field2", "Field3", subprocesses[i], "Field5");	
			// Copy over document
			var newDoc = copyDocument(wfDoc, keys, props);
			if (!newDoc) {
				debug.logln("CRITICAL", "Failed to copy document from source [%s]", wfDoc.id);
				return false;
			}
			// Create form manager
			var FORM_NAME = "DPHHS - DBrief Form";
			var wsMgr = new FormManager(FORM_NAME, {delayedSave: true});
			if (!wsMgr || wsMgr.error)
			{
				debug.logln("CRITICAL", "Failed to initialize FormManager for form [%s]\n", FORM_NAME);
				return false;
			}
			// Copy over form
			if (!wsMgr.copyForm(wfDoc, newDoc)) {
				debug.logln("CRITICAL", "Failed to copy form from source [%s]", wfDoc.id);
			}
			// Set new assignee
			var newProps = newDoc.getCustomProperties();
			for (var j = 0; j < newProps.length; j++) {
				if (newProps[j].name == "DPHHS - DBrief Current Assignee") {
					newProps[j].value = getNextAssignee(subprocesses[i]);
				}
			}
			// Route document
			createOrRouteItem(newDoc, getNextQueue(subprocesses[i]));
		}*/
	}
	catch(e)
	{
		if (!debug)
		{
			printf("\n\nFATAL iSCRIPT ERROR: %s\n\n", e.toString());
		}
		else
		{
			debug.setIndent(0);
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "**                                           **");
			debug.logln("CRITICAL", "**    ***    Fatal iScript Error!     ***    **");
			debug.logln("CRITICAL", "**                                           **");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "\n\n\n%s\n\n", e.toString());
			debug.logln("CRITICAL", "\n\nThis script has failed in an unexpected way.  Please\ncontact Hyland Software Customer Support at 800-941-7460 ext. 2");
			debug.logln("CRITICAL", "Alternatively, you may wish to email es_support@hyland.com\nPlease attach:\n - This log file");
			debug.logln("CRITICAL", " - The associated script [%s]\n - Any supporting files that might be specific to this script\n", _argv[0]);
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			if (DEBUG_LEVEL < 3 && typeof(debug.getLogHistory) === "function")
			{
				debug.popLogHistory(11);
				debug.logln("CRITICAL", "Log History:\n\n%s\n", debug.getLogHistory());
			}
		}
	}
	finally
	{
		if (debug) debug.setIndent(0);
		if (typeof(stats) == "object" && stats)
		{
			if (stats.isEmpty()) stats.inc("Stats successfully generated.");
			if (debug) debug.logAlways("NOTIFY", "Done:\n\n%s\n", stats.getSortedStats());
			else printf("Done:\n\n%s\n", stats.getSortedStats());
		}
		if (debug) debug.finish();
	}
}	

// ********************* Function Definitions **********************************

/** ****************************************************************************
  * Return the assignee from the subprocess who the approved request will go to next
  *
  * @param {String} subprocess name
  * @return {Boolean} name of assignee in next workflow
  *****************************************************************************/
function getNextAssignee(subprocess) {
	var assignee = "";
	// Determine assignee
	return assignee;
}

/** ****************************************************************************
  * Return the queue name from the subprocess workflow where the approved request
  * will be routed next
  *
  * @param {String} subprocess name
  * @return {Boolean} name of queue in next workflow
  *****************************************************************************/
function getNextQueue(subprocess) {
	var queue = "";
	// Determine queue
	return queue;
}