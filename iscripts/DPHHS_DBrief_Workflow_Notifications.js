/********************************************************************************
	
	Name:			DPHHS_DBrief_Workflow_Notifications.js
	Author:			csj070 (CMurnion)
	Created:		04/02/2020
	Last Updated:	
	For Version:	6.x/7.x
	Script Version: 
---------------------------------------------------------------------------------
	Summary:
		The script will send two emails whenever an item is routed:
		One to the requester updating them on the status of their request and
		one to the new current assignee who must now take action on the item.
		If these are the same people, only one email will be sent.
		
	Mod Summary:
		
		
	Execution Method:
		This script is designed to be run as workflow inbound.
		
********************************************************************************/

// ********************* Include additional libraries *******************
// Linked Libraries
//#link "inxml"    //XML parser
//#link "sedbc"    //Database object
#link "secomobj" //COM object

// STL packages
#if defined(imagenowDir6)
	// 6.7.0.2717+, including Active-Active support
	#include "$IMAGENOWDIR6$/script/STL2/packages/Logging/iScriptDebug.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Logging/StatsObject.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/User/findUsers.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/User/getUserProperties.js"
	#include "$IMAGENOWDIR6$/script/STL2/packages/Email/iEmail.js"
#else
	// pre-6.7.0.2717, no Active-Active support
	#include "../script/STL2/packages/Logging/iScriptDebug.js"
	#include "../script/STL2/packages/Logging/StatsObject.js"
	#include "../script/STL2/packages/User/findUsers.js"
	#include "../script/STL2/packages/User/getUserProperties.js"
	#include "../script/STL2/packages/Email/iEmail.js"
#endif

// *********************         Configuration        *******************

var CONFIG_VERIFIED     = true;   // set to true when configuration values have been verified

// Logging
var LOG_TO_FILE         = true;    // false - log to stdout if ran by intool, true - log to inserverXX/log/ directory
var DEBUG_LEVEL         = 5;       // 0 - 5.  0 least output, 5 most verbose
var SPLIT_LOG_BY_THREAD = false;   // set to true in high volume scripts when multiple worker threads are used (workflow, external message agent, etc)
var MAX_LOG_FILE_SIZE   = 100;     // Maximum size of log file (in MB) before a new one will be created

var DRY_RUN = false;

//E-mail Configuration
var EMAIL_CONFIG = {
	IsHTML		: false,
	useUtility	: false,	// if this is true, ASPEmail will need to be installed on the server running this script.
	useCDO		: false,
	smtp		: "mail.mt.gov",
	from		: "ecmdev@mt.gov",
	to			: "",
	cc			: "",
	bcc			: "", //emailConfig.bodyParams
	emailsubject		: "",
	emailbody		: "",
};

// *********************       End  Configuration     *******************

// ********************* Initialize global variables ********************
var EXECUTION_METHODS = ["WORKFLOW"]; //Allowed script execution methods: WORKFLOW, INTOOL, TASK, EFORM, EMA, INTEGRATION
var debug;

/**
 * Main body of script.
 * @method main
 * @return {Boolean} True on success, false on error.
 */
function main ()
{
	try
	{
		debug = new iScriptDebug("USE SCRIPT FILE NAME", LOG_TO_FILE, DEBUG_LEVEL, undefined, {splitLogByThreadID:SPLIT_LOG_BY_THREAD, maxLogFileSize:MAX_LOG_FILE_SIZE});
		debug.showINowInfo("INFO");
		debug.logAlways("INFO", "Script Version: $Id: SOM_BFSDJournalNotifications.js 40343 2019-04-11 08:54:05Z rhalder $\n");
		debug.logAlways("INFO", "Script Name: %s\n", _argv[0]);
		var stats = new StatsObject();

		if (!CONFIG_VERIFIED)
		{
			var errorStr = "Configuration not verified.  Please verify \n" +
			"the defines in the *** Configuration *** section at the top \n" +
			"of this script and set CONFIG_VERIFIED to true.  Aborting.\n";
			debug.logln("CRITICAL", errorStr);
			printf(errorStr);
			return false;
		}
		
		// check script execution
		if (!debug.checkExecution(EXECUTION_METHODS))
		{
			debug.logln("CRITICAL", "This iScript is running as [%s] and is designed to run from [%s]", debug.getExecutionMethod(), EXECUTION_METHODS);
			return false;
		}

		// Get the inbound workflow item
		var wfItem = new INWfItem(currentWfItem.id);
		if (!wfItem.id || !wfItem.getInfo())
		{
			debug.logln("CRITICAL", "Couldn't get info for workflow item [%s]: %s", currentWfItem.id, getErrMsg());
			return false;
		}
		
		// Get the document and custom properties from the workflow item
		wfDoc = new INDocument(wfItem.objectId);
		var docLink = wfDoc.createLink("C:\\")
		
		var props = wfDoc.getCustomProperties();
		
		// Get values from custom properties
		// Assignee and requester IDs are hidden fields on the form. If the form is at a stopping point
		// (approved or denied) assigneeID will be "process_completed"
		var assigneeID = "";
		var requesterID= "";
		var processStatus = "";
		var decisionName = "";
		var assigneeName = "";
		var assigneeEmail = "";
		var requesterEmail = "";
		
		
		for (var i = 0; i < props.length; i++) {
			if (props[i].name == "DPHHS - DBrief Current Assignee ID") {
				assigneeID = props[i].getValue();
			} else if (props[i].name == "DPHHS - DBrief Requester ID") {
				requesterID = props[i].getValue();
			} else if (props[i].name == "DPHHS - DBrief Process Status") {
				processStatus = props[i].getValue();
			} else if (props[i].name == "DPHHS - DBrief Decision Name") {
				decisionName = props[i].getValue();
			} else if (props[i].name == "DPHHS - DBrief Current Assignee") {
				assigneeName = props[i].getValue();
			} else if (props[i].name == "DPHHS - DBrief Assignee Email") {
				assigneeEmail = props[i].getValue();
			} else if (props[i].name == "DPHHS - DBrief Requester Email") {
				requesterEmail = props[i].getValue();
			}
		}
		
		if (assigneeID == "") {
			debug.logln("CRITICAL", "Couldn't get current assignee ID from document %s", wfDoc.id);
			return false;
		}
		
		if (requesterID == "") {
			debug.logln("CRITICAL", "Couldn't get requester ID from document %s", wfDoc.id);
			return false;
		}
		
		if (processStatus == "") {
			debug.logln("CRITICAL", "Couldn't get process status from document %s", wfDoc.id);
			return false;
		}
		
		if (decisionName == "") {
			debug.logln("CRITICAL", "Couldn't get decision name from document %s", wfDoc.id);
			return false;
		}
		
		if (assigneeName == "" && assigneeID != "process_completed") {
			debug.logln("CRITICAL", "Couldn't get assignee name from document %s", wfDoc.id);
			return false;
		}
		
		if (assigneeEmail == "" && assigneeID != "process_completed") {
			debug.logln("CRITICAL", "Couldn't get assignee email from document %s", wfDoc.id);
			return false;
		}
		
		if (requesterEmail == "") {
			debug.logln("CRITICAL", "Couldn't get requester email from document %s", wfDoc.id);
			return false;
		}
		
		
		// If revisions are required from the requester, send email for assignee
		if (assigneeID == requesterID && processStatus == "Revisions Required") {
			var emailToAssignee = generateEmail(processStatus, "assignee", decisionName, "", docLink);
			if (emailToAssignee === false) {
				debug.logln("CRITICAL", "Failed to generate email to user: %s", assigneeID);
				return false;
			}
			
			if (!sendEmail(requesterEmail, emailToAssignee)) {
				debug.logln("CRITICAL", "Failed to send email to %s. Review server logs.", requesterEmail);
				return false;
			}
		} else if (assigneeID != requesterID) {
			// Otherwise send email to requester like normal
			var emailToRequester = generateEmail(processStatus, "requester", decisionName, assigneeName, docLink);
			if (emailToRequester === false) {
				debug.logln("CRITICAL", "Failed to generate email to user: %s", requesterID);
				return false;
			}
			
			if (!sendEmail(requesterEmail, emailToRequester)) {
				debug.logln("CRITICAL", "Failed to send email to %s. Review server logs.", requesterEmail);
				return false;
			}
		}
		
		// Send email to current assignee
		// Only do this if requester and current assignee are not the same person (i.e. if routing is a revision request)
		// and if the form is still being processed
		if (assigneeID != requesterID && assigneeID != "process_completed") {	
			var emailToAssignee = generateEmail(processStatus, "assignee", decisionName, assigneeName, docLink);
			if (emailToAssignee === false) {
				debug.logln("CRITICAL", "Failed to generate email to user: %s", assigneeID);
				return false;
			}
			
			if (!sendEmail(assigneeEmail, emailToAssignee)) {
				debug.logln("CRITICAL", "Failed to send email to %s. Review server logs.", assigneeEmail);
				return false;
			}
		}
	}
	catch(e)
	{
		if (!debug)
		{
			printf("\n\nFATAL iSCRIPT ERROR: %s\n\n", e.toString());
		}
		else
		{
			debug.setIndent(0);
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "**                                           **");
			debug.logln("CRITICAL", "**    ***    Fatal iScript Error!     ***    **");
			debug.logln("CRITICAL", "**                                           **");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "\n\n\n%s\n\n", e.toString());
			debug.logln("CRITICAL", "\n\nThis script has failed in an unexpected way.  Please\ncontact Hyland Software Customer Support at 800-941-7460 ext. 2");
			debug.logln("CRITICAL", "Alternatively, you may wish to email es_support@hyland.com\nPlease attach:\n - This log file");
			debug.logln("CRITICAL", " - The associated script [%s]\n - Any supporting files that might be specific to this script\n", _argv[0]);
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			debug.logln("CRITICAL", "***********************************************");
			if (DEBUG_LEVEL < 3 && typeof(debug.getLogHistory) === "function")
			{
				debug.popLogHistory(11);
				debug.logln("CRITICAL", "Log History:\n\n%s\n", debug.getLogHistory());
			}
		}
	}
	finally
	{
		if (debug) debug.setIndent(0);
		if (typeof(stats) == "object" && stats)
		{
			if (stats.isEmpty()) stats.inc("Stats successfully generated.");
			if (debug) debug.logAlways("NOTIFY", "Done:\n\n%s\n", stats.getSortedStats());
			else printf("Done:\n\n%s\n", stats.getSortedStats());
		}
		if (debug) debug.finish();
	}
}	
		
		
// ********************* Function Definitions **********************************

/** ****************************************************************************
  * Send email
  *
  * @param {String} email address
  * @param {Array} email subject and body
  * @return {Boolean} true on success,false on error
  *****************************************************************************/
function sendEmail(sendTo, email)
{
	var strSubject	= email[0]
	var strBody		= email[1]
	
	var emailConfig = 
	{
		smtp:		EMAIL_CONFIG.smtp,
		to:			sendTo,
		from:		EMAIL_CONFIG.from,
		cc:			EMAIL_CONFIG.cc,
		bcc:		EMAIL_CONFIG.bcc,
		subject:	strSubject,
		body:		strBody,
		IsHTML:		EMAIL_CONFIG.IsHTML,
		useUtility:	EMAIL_CONFIG.useUtility,
	};
	
	var email = new iEmail(sendTo, emailConfig);
	
	//send e-mail
	if(!email.send())
	{
		debug.logln("ERROR", "sendEmail: Error sending email to %s: %s.",sendTo,getErrMsg());
		return false;
	}
	else
	{
		debug.logln("INFO", "sendEmail: Email sent successfully..");
	}
	return true;
}

/** ****************************************************************************
  * Determine what kind of email to send from process status
  *
  * @param {String} process status of decision request
  * @param {String} the role of the person the email is going to
  * @param {String} name of decision request
  * @param {String} name of current assignee
  * @return {Array} email[0] = subject of email; email[1] = body of email
  *****************************************************************************/
function generateEmail(processStatus, role, decisionName, assigneeName, docLink) {
	var email = [];
	var emailSubject = "";
	var emailBody = "";
	
	if (role == "assignee") {
		if (processStatus == "Further Review Required") {
			emailSubject = "Action Required on Decision Request: " + decisionName + "."
			emailBody = "Your review is needed for decision request " + decisionName + ". Please log into Perceptive Content and " +
						"take action on the request. \n Document link: " + docLink
		} else if (processStatus == "Revisions Required") {
			emailSubject = "Action Required on Decision Request: " + decisionName + "."
			emailBody = "Revisions are needed for decision request " + decisionName + ". Please log into Perceptive Content and " +
						"see the reviewer comments at the bottom of the decision request to see what revisions are required. \n Document link: " + docLink
		} else {
			debug.logln("ERROR", "Invalid process status given for assignee email: %s", processStatus);
			return false;
		}
	} else if (role == "requester") {
		if (processStatus == "Approved") {
			emailSubject = "Decision Request: " + decisionName + " Approved."
			emailBody = "Your decision request " + decisionName + " has been approved. The request will remain in workflow for 7 days " +
						"and then be archived. You will receive additional notifications if the request must be routed through additional " +
						"business processes. \n Document link: " + docLink
		} else if (processStatus == "Further Review Required") {
			emailSubject = "Status Update on Decision Request: " + decisionName 
			emailBody = "Your decision request " + decisionName + " has been routed to " + assigneeName + " for additional review. Please log " +
						"into Perceptive Content to view recent comments on your decision request. \n Document link: " + docLink
		} else if (processStatus == "Revisions Required") {
			emailSubject = "Status Update on Decision Request: " + decisionName 
			emailBody = "Your decision request " + decisionName + " has been routed to " + assigneeName + " for revisions. Please log " +
						"into Perceptive Content to view recent comments on your decision request. \n Document link: " + docLink
		} else if (processStatus == "Denied") {
			emailSubject = "Decision Request: " + decisionName + " Denied."
			emailBody = "Your decision request " + decisionName + " has been denied. The request will remain in workflow for 7 days " +
						"and then be removed. Please log into Perceptive Content to view comments on your decision request. \n Document link: " + docLink
		} else {
			debug.logln("ERROR", "Invalid process status given for requester email: %s", processStatus);
			return false;
		}
	} else {
		debug.logln("ERROR", "Invalid email recipient role given: %s", role);
		return false;
	}
	
	email.push(emailSubject);
	email.push(emailBody);
	return email;
}