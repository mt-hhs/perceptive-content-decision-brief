//Scripts for the Decision Brief Perceptive form
//More documentation to come

// Public variables
var allObjectives;
var allStrategies;
// Used to keep track if the user has clicked outside of the search results
var windowClicks = 0;
// Set to the results of a web service call (displaySearchResults)
var emplSearchResults;
var notifyEmail = "HHSApplicationDevelopment@mt.gov";

var showReviewerComments = false;


//Populates some fields when loading the form
function loadForm() {
	hideEmptyComment();
	initCheckBoxes();
	showRouting();
	populateDate();
	//allObjectives = toArray(searchByClass("js-objective-select")[0].options);
	allObjectives = toArray(document.getElementById("objectiveSelect").options);
	//allStrategies = toArray(searchByClass("js-strategy-select")[0].options);
	allStrategies = toArray(document.getElementById("strategySelect").options);
	populateObjectives();
	populateStrategies();
	determineCommentsDisplay();
}

//Comments are hidden the first time the document is opened. Unhide them for subsequent openings.
//The hidden checkbox showCommentsCB is used to mark whether to display the comments.
function determineCommentsDisplay()
{
	if (document.getElementById("showCommentsCB").checked)
		document.getElementById("reviewerComments").style.display = "block";
	else
		document.getElementById("showCommentsCB").checked = true;
}

//Returns an array of all elements with the given class.
//Start at the document root and recursively search through all children,
//adding the nodes with the given class to an array.
function searchByClass (elemClass) {
	var searchResults = [];
	
	recursiveSearchByClass(elemClass, document, searchResults);
	
	return searchResults;
}

function recursiveSearchByClass (elemClass, currentNode, resultsArray) {
	//If this node has the class, add it to the array.
	if (currentNode.className == elemClass) {
		resultsArray.push(currentNode);
	}
	
	//If this node has children, recursively call the function for each child.
	if (currentNode.hasChildNodes()) {
		for (var i = 0; i < currentNode.childNodes.length; i++) {
			recursiveSearchByClass(elemClass, currentNode.childNodes[i], resultsArray);
		}
	}
}

// Returns the index position of the given element in the searchByClass array
function getPositionInArray(elem) {	
	for (var i = 0; i < searchByClass(elem.className).length; i++) {
		if (searchByClass(elem.className)[i] === elem) {
			return i;
		}
	}
}

//Converts an HTMLCollection object into a JavaScript array
function toArray(collection) {
	var array = [];
    for(var i = 0; i < collection.length; i++) {
        array.push(collection[i]);		
	}
    return array
} 

//Sets all check box options to "no" by default.
function initCheckBoxes() {
	if (document.getElementById("tribalImpact").selectedIndex != 1) {
		document.getElementById("tribalImpact").selectedIndex = 0;
	}
	if (document.getElementById("fiscalImpact").selectedIndex != 1) {
		document.getElementById("fiscalImpact").selectedIndex = 0;
	}
	if (document.getElementById("smallBusinessImpact").selectedIndex != 1) {
		document.getElementById("smallBusinessImpact").selectedIndex = 0;
	}
	if (document.getElementById("medicaidImpact").selectedIndex != 1) {
		document.getElementById("medicaidImpact").selectedIndex = 0;
	}
}

//Show routing decisions in reviewer comments on form load
function showRouting() {
	for (var i = 0; i < searchByClass("js-route-type-row").length; i++) {
		if (searchByClass("js-route-type")[i].selectedIndex != 0) {
			searchByClass("js-route-type-row")[i].style.display = "block";
		} else if (searchByClass("js-route-type")[i].selectedIndex == 0 && searchByClass("js-route-type")[i].id == "submitRouteType") {
			searchByClass("js-route-type-row")[i].disabled = false;
		}
	}
	
	for (var j = 0; j < searchByClass("js-route-to-row").length; j++) {
		if (searchByClass("js-route-to")[j].value != "") {
			searchByClass("js-route-to-row")[j].style.display = "block";
		} else if (searchByClass("js-route-to")[j].value == "" && searchByClass("js-route-to")[j].id == "submitRouteTo") {
			searchByClass("js-route-to")[j].readOnly = false;
		}
	}
}

//Sets the hidden business impact select elements based on the state of the associated checkbox
function businessImpactClick(elem) {
	if (elem.id == "tribalcb") {
		if (elem.checked) {
			document.getElementById("tribalImpact").selectedIndex = 1;
		} else {
			document.getElementById("tribalImpact").selectedIndex = 0;
		}
	} else if (elem.id == "fiscalcb") {
		if (elem.checked) {
			document.getElementById("fiscalImpact").selectedIndex = 1;
		} else {
			document.getElementById("fiscalImpact").selectedIndex = 0;
		}
	} else if (elem.id == "smallbusinesscb") {
		if (elem.checked) {
			document.getElementById("smallBusinessImpact").selectedIndex = 1;
		} else {
			document.getElementById("smallBusinessImpact").selectedIndex = 0;
		}
	} else if (elem.id == "medicaidcb") {
		if (elem.checked) {
			document.getElementById("medicaidImpact").selectedIndex = 1;
		} else {
			document.getElementById("medicaidImpact").selectedIndex = 0;
		}
	}
}

//Sets the hidden process involved select elements based on the state of the associated checkbox
function processInvolvedClick(elem) {
	if (elem.id == "armcb") {
		if (elem.checked) {
			document.getElementById("armInvolved").selectedIndex = 1;
		} else {
			document.getElementById("armInvolved").selectedIndex = 0;
		}
	} else if (elem.id == "spacb") {
		if (elem.checked) {
			document.getElementById("spaInvolved").selectedIndex = 1;
		} else {
			document.getElementById("spaInvolved").selectedIndex = 0;
		}
	} else if (elem.id == "mcacb") {
		if (elem.checked) {
			document.getElementById("mcaInvolved").selectedIndex = 1;
		} else {
			document.getElementById("mcaInvolved").selectedIndex = 0;
		}
	} else if (elem.id == "waivercb") {
		if (elem.checked) {
			document.getElementById("waiverInvolved").selectedIndex = 1;
		} else {
			document.getElementById("waiverInvolved").selectedIndex = 0;
		}
	} else if (elem.id == "procedurecb") {
		if (elem.checked) {
			document.getElementById("procedureInvolved").selectedIndex = 1;
		} else {
			document.getElementById("procedureInvolved").selectedIndex = 0;
		}
	} else if (elem.id == "policycb") {
		if (elem.checked) {
			document.getElementById("policyInvolved").selectedIndex = 1;
		} else {
			document.getElementById("policyInvolved").selectedIndex = 0;
		}
	} else if (elem.id == "othercb") {
		if (elem.checked) {
			document.getElementById("otherInvolved").selectedIndex = 1;
		} else {
			document.getElementById("otherInvolved").selectedIndex = 0;
		}
	}
}

//Constructs the request name as a concatenation of requesting division, request title, and date submitted
function setRequestName() {
	var division = document.getElementById("requestingDivision");
	document.getElementById("requestName").value = division.children[division.selectedIndex].text +
												   document.getElementById("requestTitle").value +
												   document.getElementById("dateSubmitted").value;
}

//Populate the date submitted with the current date if it is null
function populateDate() {
	
	var currentDate = new Date();
	var formDate = document.getElementById("dateSubmitted");
	
	if (formDate.value == "") {
		formDate.value = ((currentDate.getMonth() + 1) +
						 "/" + currentDate.getDate() + 
						 "/" + currentDate.getFullYear()).replace(" ", "");
	}
}

//Populate objective choices when the form is loaded
function populateObjectives() {
	var goalSelectArray = searchByClass("js-goal-select");
	var objectiveSelectArray = searchByClass("js-objective-select");
	var objectiveIndexArray = searchByClass("js-objective-index");
	
	
	for (var i = 0; i < goalSelectArray.length; i++) {
		setObjectiveOptions(goalSelectArray[i]);
		objectiveSelectArray[i].selectedIndex = objectiveIndexArray[i].value;
	}
}

//Populate strategy choices when the form is loaded
function populateStrategies() { 
	var objectiveSelectArray = searchByClass("js-objective-select");
	var strategySelectArray = searchByClass("js-strategy-select");
	var strategyIndexArray = searchByClass("js-strategy-index");
	
	for (var i = 0; i < objectiveSelectArray.length; i++) {
		setStrategyOptions(objectiveSelectArray[i]);
		strategySelectArray[i].selectedIndex = strategyIndexArray[i].value;
	}
}

//An onclick function that calls two other functions when the objective is changed
function setObjective(elem) {
	setObjectiveIndex(elem);
	setStrategyOptions(elem);
}

//Set the hidden index element for objective choices
function setObjectiveIndex(elem) {
	var objectiveSelectArray = searchByClass("js-objective-select");
	var objectiveIndexArray = searchByClass("js-objective-index");
	
	var position;
	
	for (var i = 0; i < objectiveSelectArray.length; i++) {
		if (objectiveSelectArray[i] === elem) {
			position = i;
		}
	}
	
	objectiveIndexArray[position].value = elem.selectedIndex;
}
/* 
input: goal select element
output: none

Populate the strategic objective field based on the goal selection.
The options that do not align with the previously-selected goal are then disabled.
*/
function setObjectiveOptions(elem) {	
	var selectedGoal = elem.options[elem.selectedIndex].text;
	
	var position = getPositionInArray(elem);
	
	var objectiveSelect = searchByClass("js-objective-select")[position];
	
 	//Remove all previous options
	while (objectiveSelect.options.length) {
		objectiveSelect.options.remove(0);
	}

	//Add all available options
	var i;
	for (i = 0; i < allObjectives.length; i++) {
		var newOption = new Option(allObjectives[i].text, false, false);
		objectiveSelect.options.add(newOption);
	}
	
	var j;
	//Remove all unmatched options
	for (j = 0; j < objectiveSelect.options.length; j++) {
		if (objectiveSelect.options[j].text.slice(0,1) != selectedGoal.slice(0,1) && objectiveSelect.options[j].text != "") {
			objectiveSelect.options.remove(j);
			j--;
		}
	}
	
	//objectiveSelect.selectedIndex = 0;
}

//Set the hidden strategy index element when strategy choices change
function setStrategyIndex(elem) {
    var strategySelectArray = searchByClass("js-strategy-select");
	var strategyIndexArray = searchByClass("js-strategy-index");
	
	var position;
	
	for (var i = 0; i < strategySelectArray.length; i++) {
		if (strategySelectArray[i] === elem) {
			position = i;
		}
	}
	
	strategyIndexArray[position].value = elem.selectedIndex;
}

/* 
input: goal select element
output: none

Populate the strategy field based on the objective  selection.
Works similarly to populateObjectives
*/
function setStrategyOptions(elem) {
	var selectedObjective = elem.options[elem.selectedIndex].text;
	
	var position = getPositionInArray(elem);
	
	var strategySelect = searchByClass("js-strategy-select")[position];
	
	//Remove all previous options
	while (strategySelect.options.length) {
		strategySelect.options.remove(0);
	}
	
	//Add all available options
	var i;
	for (i = 0; i < allStrategies.length; i++) {
		var newOption = new Option(allStrategies[i].text, false, false);
		strategySelect.options.add(newOption);
	}
	
	var j;
	//Remove all unmatched options
	for (j = 0; j < strategySelect.options.length; j++) {
		if (strategySelect.options[j].text.slice(0,3) != selectedObjective.slice(0,3) && strategySelect.options[j].text != "") {
			strategySelect.options.remove(j);
			j--;
		}
	}
	
	strategySelect.selectedIndex = 0;
}

// Deletes a row from the strategic plan table
function deletePlanRow(elem) {
	if (searchByClass("js-goal-select").length == 1) {
		document.getElementById("goalSelect").selectedIndex = 0;
		document.getElementById("objectiveSelect").selectedIndex = 0;
		document.getElementById("strategySelect").selectedIndex = 0;
	} else {
		var i = elem.parentNode.parentNode.rowIndex;
		document.getElementById("planTable").deleteRow(i);
	}
}

//Hides the comment section on form load if there are no comments
function hideEmptyComment() {
	if (searchByClass("js-comment-timestamp").length == 1 && searchByClass("js-comment-timestamp")[0].value == "") {
		document.getElementById("commentTable").style.display = "none";
		document.getElementById("addComment").style.display = "block";
	}
}

//Makes the first reviewer comment row visible and make invisible the button used to add it
function addFirstComment() {
	addCommentTimestamp();
	document.getElementById("commentTable").style.display = "block";
	document.getElementById("addComment").style.display = "none";
}

//Use a time delay so that the new table row is created before the timestamp is added
function addCommentClick() {
	setTimeout(addCommentTimestamp, 10);
}

//Adds a timestamp to a newly-created comment and opens it for editing
function addCommentTimestamp() {
	var currentTime = new Date();
	var last = searchByClass("js-comment-timestamp").length - 1;
	
	searchByClass("js-comment-timestamp")[last].value = currentTime;
	searchByClass("js-reviewer-name")[last].readOnly = false;
	searchByClass("js-reviewer-comment")[last].readOnly = false;
	searchByClass("js-recommendation-decision")[last].readOnly = false;
	searchByClass("js-action-select")[last].disabled = false;
	searchByClass("js-route-type-row")[last].style.display = "none";
	searchByClass("js-route-to-row")[last].style.display = "none";
}

//Deletes a reviewer comment row. If it is the only comment, reset all the comment fields
function deleteCommentRow(elem) {
	if (searchByClass("js-comment-timestamp").length == 1) {
		document.getElementById("reviewerName").value = "";
		document.getElementById("commentTimestamp").value = "";
		document.getElementById("reviewerComment").value = "";
		document.getElementById("recommendationDecision").value = "";
		document.getElementById("actionSelect").selectedIndex = 0;
		document.getElementById("commentTable").style.display = "none";
		document.getElementById("addComment").style.display = "block";
		document.getElementById("routeButton").style.display = "none";
	} else {
		var i = elem.parentNode.parentNode.rowIndex;
		document.getElementById("commentTable").deleteRow(i);
	}
}

//Set process status of the document based on reviewer action selected in comment section
function setProcessStatus(elem) {
	var position = getPositionInArray(elem);
	searchByClass("js-route-type-row")[position].style.display = "none";
	searchByClass("js-route-type")[position].selectedIndex = 0;
	searchByClass("js-route-to-row")[position].style.display = "none";
	searchByClass("js-route-to")[position].value = "";
	
	// If an option is selected, show the 'Submit' button
	if (elem.selectedIndex != 0) {
		document.getElementById("processStatus").selectedIndex = elem.selectedIndex;
		document.getElementById("routeButton").style.display = "block";
	} else {
		document.getElementById("routeButton").style.display = "none";
	}
	
	// If 'Further Review Requested' is selected, display the 'Route Type' and 'Route To' field
	if (elem.selectedIndex == 2) {
		searchByClass("js-route-type-row")[position].style.display = "block";
		searchByClass("js-route-type")[position].disabled = false;
		searchByClass("js-route-to-row")[position].style.display = "block";
		searchByClass("js-route-to")[position].readOnly = false;
	// If 'Revisions Required' is selected, display 'Route To' field
	} else if (elem.selectedIndex == 3) {
		searchByClass("js-route-to-row")[position].style.display = "block";
		searchByClass("js-route-to")[position].readOnly = false;
	}		
}

// If the user selects the automatic route to supervisor, this function uses the results of the 
// hierarchy web service to supply the name of the user's direct supervisor.
function getSupervisorName(elem) {
	var position = getPositionInArray(elem);
	var sabhrsId;
	// If this is the first submission of the form, use the requester ID to get the supervisor
	if (document.getElementById("hdnAssigneeID").value == "") {
		sabhrsId = document.getElementById("hdnRequesterID").value;
	}
	// Otherwise use the assignee ID to get supervisor
	else {
		sabhrsId = document.getElementById("hdnAssigneeID").value;
	}
	
	if (sabhrsId == "") {
		alert("Please enter requester information before trying to route form.");
	} else {
		// If the option selected is 'Automatic Route', then go ahead and populate the 'route to' field
		if (elem.selectedIndex == 1) {
			hierarchyCall(sabhrsId).done(function(results) {
				searchByClass("js-route-to")[position].value = results[0].approver1FirstName + " " + results[0].approver1LastName;
				document.getElementById("hdnRoutingID").value = results[0].approver1SabhrsId;
				document.getElementById("hdnRoutingEmail").value = results[0].approver1Email;
			});
		// Otherwise blank out the routing fields
		} else {
			searchByClass("js-route-to")[position].value = "";
			document.getElementById("hdnRoutingID").value = "";
			document.getElementById("hdnRoutingEmail").value = "";
		}
	}
}

// Sets values once the "Route" button is clicked. Routes the item based on the ID of the current assignee
function routeRequest() {
	// Still need to add clicking of an iScript button (and the iScript itself) in order to actually route this item	
	if (document.getElementById("processStatus").selectedIndex == 2 || document.getElementById("processStatus").selectedIndex == 3) {
		// If Review Required or Revisions selected, update hidden assignee values
		document.getElementById("hdnAssigneeID").value = document.getElementById("hdnRoutingID").value;
		document.getElementById("hdnAssigneeEmail").value = document.getElementById("hdnRoutingEmail").value;
		
		// Update reviewer list
		if (searchByClass("js-hdn-reviewer-name")[0].value != "") {
			// If this isn't the first reviewer, add a new row to the hidden reviewers field - it needs a little time to get created so it can be populated
			document.getElementById("reviewerButton").click();
		}
		setTimeout(function() {
			searchByClass("js-hdn-reviewer-name")[searchByClass("js-hdn-reviewer-name").length - 1].value =  searchByClass("js-route-to")[searchByClass("js-route-to").length - 1].value;
			document.getElementById("currentAssignee").value = searchByClass("js-route-to")[searchByClass("js-route-to").length - 1].value;	
		}, 10);

	} else if (document.getElementById("processStatus").selectedIndex == 1 || document.getElementById("processStatus").selectedIndex == 4) {
		// Else if process status is approved or rejected, set assignee ID to process_completed
		document.getElementById("currentAssignee").value = "";
		document.getElementById("hdnAssigneeID").value = "process_completed";
	}
}

// Shading on hover-over for search results
function hoverOnSearch(elem) {
	elem.style.backgroundColor = "#d9d9d9";
}

function hoverOffSearch(elem) {
	elem.style.backgroundColor = "#f1f1f1";
}

// Displays a dropdown list of search results when the search icon is pressed
// Uses a web service to do a lookup of all employees that have a full name matching the search string
function displaySearchResults(elem) {
	var rowName = elem.id.replace("SearchIcon", "");
	var searchString;
	var routeTo;
	var dropdown;
	
	// If this is a search in the routing field in the comments, it has to be handled differently because
	// there may be multiple identical fields.
	if (rowName == "route") {
		// Find the routeTo field for this row in the multirow comment table
		for (var i = 0; i < elem.parentNode.childNodes.length; i++) {
			if (elem.parentNode.childNodes[i].className == "js-route-to") {
				routeTo = elem.parentNode.childNodes[i];
			}
		}
		searchString = routeTo.value;
		
		// Find the dropdown for this row in the multirow comment table
		for (var i = 0; i < elem.parentNode.childNodes.length; i++) {
			if (elem.parentNode.childNodes[i].className == "dropdown") {
				dropdown = elem.parentNode.childNodes[i].firstChild;
			}
		}
	} else {
		// Otherwise the fields will have unique IDs
		searchString = document.getElementById(rowName + "NameInput").value;
		dropdown = document.getElementById(rowName + "Dropdown");
	}
	
	// Web service call to get all employee names matching an input string
	searchCall(searchString).done(function(results) { 
		emplSearchResults = results;
		
		// If only one result is returned, automatically set the fields to that result
		if (emplSearchResults.length == 1) {
			if (rowName != "route") {
				document.getElementById(rowName + "NameInput").value = emplSearchResults[0].employeeFirstName + " " + emplSearchResults[0].employeeLastName;
				document.getElementById(rowName + "PhoneInput").value = emplSearchResults[0].employeePhone;
				document.getElementById(rowName + "EmailInput").value = emplSearchResults[0].employeeEmail;
			
				if (rowName == "requester") {
					document.getElementById("hdnRequesterID").value = emplSearchResults[0].employeeSabhrsId;
				}
			} else {
				routeTo.value = emplSearchResults[0].employeeFirstName + " " + emplSearchResults[0].employeeLastName;
				document.getElementById("hdnAssigneeID").value = emplSearchResults[0].employeeSabhrsId;
				document.getElementById("hdnAssigneeEmail").value = emplSearchResults[0].employeeEmail;
			}
			return;
		}
		
		// Otherwise, put the search results in a dropdown list
		
		var cloneMe = dropdown.firstChild.cloneNode();
		
		// Remove all previous name choices
		while (dropdown.hasChildNodes()) {  
			dropdown.removeChild(dropdown.firstChild);
		}
		
		// Add the results of the current search to the dropdown
		for (var i = 0; i < emplSearchResults.length; i++) {
			
			var para = cloneMe.cloneNode();
			
			if (emplSearchResults[i].employeeDivision != null) {
				// Add in division if applicable
				para.innerHTML = emplSearchResults[i].employeeFirstName + " " + emplSearchResults[i].employeeLastName +
								 " - " + emplSearchResults[i].employeeDivision + " - " + emplSearchResults[i].employeeBureau;
			} else {
				para.innerHTML = emplSearchResults[i].employeeFirstName + " " + emplSearchResults[i].employeeLastName +
								 " - " + emplSearchResults[i].employeeBureau;
			}
			
			dropdown.appendChild(para);
		}
		
		// Show the dropdown
		dropdown.style.display = "block";
	});
}

// Set fields based on which search result was clicked from the dropdown search options
function clickSearchResult(elem) {
	var searchChoices = elem.parentNode.childNodes;
	var rowName = elem.parentNode.id.replace("Dropdown", "");
    var routeTo;
	var dropdown;
	
	// If this is a search in the routing field in the comments, it has to be handled differently because
	// there may be multiple identical fields.
	if (rowName == "route") {
		// Find the routeTo field for this row in the multirow comment table
		for (var j = 0; j < elem.parentNode.parentNode.parentNode.childNodes.length; j++) {
			if (elem.parentNode.parentNode.parentNode.childNodes[j].className == "js-route-to") {
				routeTo = elem.parentNode.parentNode.parentNode.childNodes[j];
			}
		}
		dropdown = elem.parentNode;
	} else {
		// Otherwise the fields will have unique IDs
		dropdown = document.getElementById(rowName + "Dropdown");
	}
	
	var i = 0;
	// Look in all the search results
	for (i; i < searchChoices.length; i++) {
		// Find the index of the result that the user clicked on
		if (searchChoices[i] == elem) {
			// Use that index number to grab data from the global search results array
			if (emplSearchResults[i].employeePhone == null) {
				emplSearchResults[i].employeePhone = "Not found";
			}
			if (emplSearchResults[i].employeeEmail == null) {
				emplSearchResults[i].employeeEmail = "Not found";
			}
			if (rowName != "route") {
				document.getElementById(rowName + "NameInput").value = emplSearchResults[i].employeeFirstName + " " + emplSearchResults[i].employeeLastName;
				document.getElementById(rowName + "PhoneInput").value = emplSearchResults[i].employeePhone;
				document.getElementById(rowName + "EmailInput").value = emplSearchResults[i].employeeEmail;
				
				if (rowName == "requester") {
					document.getElementById("hdnRequesterID").value = emplSearchResults[i].employeeSabhrsId;
					document.getElementById("hdnRequesterEmail").value = emplSearchResults[i].employeeEmail;
				}
			} else {
				var routeTo;
				// Find the routeTo field for this row in the multirow table

				// Set fields
				routeTo.value = emplSearchResults[i].employeeFirstName + " " + emplSearchResults[i].employeeLastName;
				document.getElementById("hdnAssigneeID").value = emplSearchResults[i].employeeSabhrsId;
				document.getElementById("hdnAssigneeEmail").value = emplSearchResults[i].employeeEmail;
			}
		}
	}
	
	// Hide dropdown
	dropdown.style.display = "none";
}

// Closes dropdown if the user clicks outside the dropdown area
function closeDropdown() {
	for (var i = 0; i < searchByClass("dropdowncontent").length; i++) {
		searchByClass("dropdowncontent")[i].style.display = "none";
	}
}

// Makes a call to a web service to get the user's direct supervisor's information
function hierarchyCall(sabhrsId){
	jQuery.support.cors = true;
    var $webMethod = "https://hhsdwg5001.hhs.mt.gov:8204/employeeHierarchy/employeeHierarchy/position/{employeeId}/{loginId}";
    var $parameters = "employeeId=" + sabhrsId;

    return $.ajax({
        type: "GET",
        url: $webMethod,
        data: $parameters,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function(){
            alert("An unexpected error occurred. Please contact ADSS support at " + notifyEmail + " for assistance.");
        }
    });
}

// Makes a call to a web service to get the results of a user's search for employee full name
function searchCall(searchString){
	jQuery.support.cors = true;
	var $webMethod = "https://hhsdwg5001.hhs.mt.gov:8204/employeeHierarchy/employee/position/{employeeName}";
    var $parameters = "employeeName=" + searchString;

    return $.ajax({
        type: "GET",
        url: $webMethod,
        data: $parameters,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function(){
            alert("An unexpected error occurred. Please contact ADSS support at " + notifyEmail + " for assistance.");
        }
    });
}