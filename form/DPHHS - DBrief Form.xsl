<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/form">
        <html>
            <head xmlns="http://www.w3.org/1999/xhtml">
                <meta name="generator"
                    content="HTML Tidy for HTML5 for Windows version 5.5.37">
                </meta>
                <title>DPHHS - Decision Request Form</title>
                <meta name="GENERATOR" content="MSHTML 11.00.10570.1001">
                </meta>
                <meta name="GENERATOR" content="MSHTML 11.00.10570.1001">
                </meta>
                <meta name="GENERATOR" content="MSHTML 11.00.10570.1001">
                </meta>
                <meta content="text/html; charset=utf-8"
                    http-equiv="Content-Type">
                </meta>
                <meta name="GENERATOR" content="MSHTML 11.00.10570.1001">
                </meta>
                <link rel="stylesheet" href="Forms.css">
                </link>
				
                <script
					src="https://code.jquery.com/jquery-1.12.4.js"
					integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
					crossorigin="anonymous">
				</script>
                <script language="JavaScript" src="DPHHS - DBrief Form.js"></script>
            </head>
            <body xmlns="http://www.w3.org/1999/xhtml" onload="loadForm();">
			
                <table class="one-col-section" style="border: 1px solid black" onclick="closeDropdown();">
                    <tbody>
                        <tr>
                            <td class="sectionheader">
                                <div>
                                    <img src="DPHHS Logo Color 150x.jpg" style="float: right;"/>
                                    <br/>
                                    <p class="title">DPHHS - Decision Request Form</p>
                                </div>
                                <table style="border: 1px solid black">
                                    <tbody>
                                        <tr class="singlepropertyrow">
                                            <td class="propertylabel">Request Name</td>
                                            <td class="propertycontent" style="WIDTH: 50%">
                                                <input id="requestName" class="propertytextinput" style="WIDTH: 100%"
                                                    title="This field is automatically populated. No action required.">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="_321Z523_0VTP8PHFG0009DC_" />
                                                </xsl:attribute>
                                                </input>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table style="border: 1px solid black">
                                    <tbody>
                                        <tr class="singlepropertyrow">
                                            <td class="propertylabel">Request Title</td>
                                            <td class="propertycontent" style="WIDTH: 50%">
                                                <input id="requestTitle" class="propertytextinput" style="WIDTH: 100%" onchange="setRequestName();"
                                                    title="Input specific name of the business issue that the decision request will address or the change requested for an ARM and the title of the rule.">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="_321Z525_0VTPFYHRB00027T_" />
                                                </xsl:attribute>
                                                </input>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table>
                                    <tr>
                                        <td style="width: 50%">
                                            <table style="border: 1px solid black">
                                                <tbody>
                                                    <tr class="singlepropertyrow">
                                                        <td class="propertylabel">Process Status</td>
                                                        <td class="propertycontent" style="WIDTH: 50%" 
															title="The current status of the decision request.">
                                                            <select id="processStatus" style="WIDTH: 100%">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="_321Z525_0VTPFFHRB00087R_" />
                                                                </xsl:attribute>
                                                                <option id="designer.infd_blank"
                                                                    value=""></option>
                                                                <option value="321Z525_0VTPFFHRB00087S">
                                                                    Approved
                                                                </option>
                                                                <option value="321Z525_0VTPFFHRB00087T">
                                                                    Further Review Required
                                                                </option>
                                                                <option value="321Z525_0VTPFFHRB00087V">
                                                                    Revisions Required
                                                                </option>
                                                                <option value="321Z525_0VTPFFHRB00087W">
                                                                    Denied
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="width: 50%">
                                            <table style="border: 1px solid black">
                                                <tbody>
                                                    <tr class="singlepropertyrow">
                                                        <td class="propertylabel">Currently Assigned To:</td>
                                                        <td class="propertycontent" style="WIDTH: 50%">
                                                            <input id="currentAssignee" type="text" readonly="readonly" style="width: 100%"
                                                                title="The person who must take action on the decision request next.">
																<xsl:attribute name="value">
																	<xsl:value-of select="_321Z525_0VTPFMHRB00022Q_" />
																</xsl:attribute>
                                                            </input>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td style="width: 50%">
                                            <table style="border: 1px solid black">
                                                <tbody>
                                                    <tr class="singlepropertyrow">
                                                        <td class="propertylabel">Requesting Division</td>
                                                        <td class="propertycontent" style="WIDTH: 50%"
															title="Choose the DPHHS division from which this decision request originated.">
                                                            <select id="requestingDivision" style="WIDTH: 100%" onchange="setRequestName();">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="_321Z523_0VTP8WHFG0008C3_" />
                                                                </xsl:attribute>
                                                                <option id="designer.infd_blank"
                                                                    value=""></option>
                                                                <option value="321Z523_0VTP8WHFG0008C4">01
                                                                    DETD
                                                                </option>
                                                                <option value="321Z523_0VTP8WHFG0008C5">02
                                                                    HCSD
                                                                </option>
                                                                <option value="321Z523_0VTP8WHFG0008C6">03
                                                                    CFSD
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EG">04
                                                                    DO
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EH">05
                                                                    CSED
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EJ">06
                                                                    BFSD
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EK">07
                                                                    PHSD
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EL">08
                                                                    QAD
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EM">09
                                                                    TSD
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EN">10
                                                                    DSD
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EP">11
                                                                    HRD
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EQ">12
                                                                    MHS
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002ER">16
                                                                    MFH
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002ES">22
                                                                    SLTC
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002ET">25
                                                                    ECFSD
                                                                </option>
                                                                <option value="321Z525_0VTPFNHRB0002EV">33
                                                                    AMDD
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="width: 50%">
                                            <table style="border: 1px solid black">
                                                <tbody>
                                                    <tr class="singlepropertyrow">
                                                        <td class="propertylabel">Date Submitted</td>
                                                        <td class="propertycontent" style="WIDTH: 50%">
                                                            <input class="propertytextinput" id="dateSubmitted" style="WIDTH: 100%" onchange="setRequestName();"
																title="Date that the decision request was originally submitted. Populated automatically.">
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="_321Z523_0VTP8HHFG0007V5_" />
                                                            </xsl:attribute>
                                                            </input>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50%">
                                            <table style="border: 1px solid black">
                                                <tbody>
                                                    <tr class="singlepropertyrow">
                                                        <td class="propertylabel">Importance</td>
                                                        <td class="propertycontent" style="WIDTH: 50%"
															title="Select the importance of this decision request to the division. Hover over choices for more information.">
                                                            <select style="WIDTH: 100%">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="_321Z525_0VTPFVHRB0002K1_" />
                                                                </xsl:attribute>
                                                                <option id="designer.infd_blank"
                                                                    value=""></option>
                                                                <option value="321Z525_0VTPFVHRB0002K2"
																	title="Select if this decision request is mandated by law, the governor, or the director.">
                                                                    Must Do
                                                                </option>
                                                                <option value="321Z525_0VTPFVHRB0002K3"
																	title="Select if this decision request improves effectiveness or efficiency of business function or improves client outcomes.">
                                                                    Should Do
                                                                </option>
                                                                <option value="321Z525_0VTPFVHRB0002K4"
																	title="Select if this decision request is administrative in nature.">
                                                                    Would Like To Do
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="width: 50%">
                                            <table style="border: 1px solid black">
                                                <tbody>
                                                    <tr class="singlepropertyrow">
                                                        <td class="propertylabel">Exception Status</td>
                                                        <td class="propertycontent" style="WIDTH: 50%"
															title="Select if this decision request has a special exception status. Hover over choices for more information.">
                                                            <select style="WIDTH: 100%">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="_321Z525_0VTPFDHRB000854_" />
                                                                </xsl:attribute>
                                                                <option id="designer.infd_blank"
                                                                    value=""></option>
                                                                <option value="321Z525_0VTPFDHRB000855"
																	title="Select this option for any ARM change that is outside the standard business process timeline.">
                                                                    Emergency
                                                                </option>
                                                                <option value="321Z525_0VTPFDHRB000856"
																	title="Select this option for a decision request that does not have an end date.">
                                                                    Extended Project
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <hr>
                                </hr>
                                <table style="border: 1px solid black">
                                    <thead>
                                        <tr>
                                            <th>ROLE</th>
                                            <th>NAME</th>
                                            <th>PHONE</th>
                                            <th>EMAIL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="singlepropertyrow">
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <select class="contactinput">
                                                    <xsl:attribute name="value">
                                                        <xsl:value-of select="RequesterRole"/>
                                                    </xsl:attribute>
                                                    <option/>
                                                    <option>Executive Sponsor</option>
                                                    <option>Requester</option>
                                                    <option>On Behalf Of</option>
                                                    <option>Requester of Rule</option>
                                                </select>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
												<img src="search icon.png" id="requesterSearchIcon" style="float: right; width: 25px; height: 25px;" 
													onclick="displaySearchResults(this);"/>
                                                <input type="text" id="requesterNameInput" class="contactinput">
													<xsl:attribute name="value">
														<xsl:value-of select="_321Z525_0VTPFEHRB000809_"/>
													</xsl:attribute>
                                                </input>
												<div class="dropdown">
													<div id="requesterDropdown" class="dropdowncontent">
														<a onclick="clickSearchResult(this);" onmouseenter="hoverOnSearch(this);" onmouseleave="hoverOffSearch(this);"/>
													</div>
												</div>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <input type="text" id="requesterPhoneInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="RequesterPhone"/>
                                                </xsl:attribute>
                                                </input>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <input type="text" id="requesterEmailInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="RequesterEmail"/>
                                                </xsl:attribute>
                                                </input>							
                                            </td>
                                        </tr>
                                        <tr class="singlepropertyrow">
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <select class="contactinput">
                                                    <xsl:attribute name="value">
                                                        <xsl:value-of select="SponsorRole"/>
                                                    </xsl:attribute>
                                                    <option/>
                                                    <option>Program Attorney</option>
                                                    <option>Project Sponsor</option>
                                                </select>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
												<img src="search icon.png" id="sponsorSearchIcon" style="float: right; width: 25px; height: 25px;" 
													onclick="displaySearchResults(this);"/>
                                                <input type="text" id="sponsorNameInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="SponsorName"/>
                                                </xsl:attribute>
                                                </input>
												<div class="dropdown">
													<div id="sponsorDropdown" class="dropdowncontent">
														<a onclick="clickSearchResult(this);" onmouseenter="hoverOnSearch(this);" onmouseleave="hoverOffSearch(this);"/>
													</div>
												</div>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <input type="text" id="sponsorPhoneInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="SponsorPhone"/>
                                                </xsl:attribute>
                                                </input>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <input type="text" id="sponsorEmailInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="SponsorEmail"/>
                                                </xsl:attribute>
                                                </input>							
                                            </td>
                                        </tr>
                                        <tr class="singlepropertyrow">
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                Project Manager
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
												<img src="search icon.png" id="managerSearchIcon" style="float: right; width: 25px; height: 25px;" 
													onclick="displaySearchResults(this);"/>
                                                <input type="text" id="managerNameInput" class="contactinput">
													<xsl:attribute name="value">
														<xsl:value-of select="ManagerName"/>
													</xsl:attribute>
                                                </input>
												<div class="dropdown">
													<div id="managerDropdown" class="dropdowncontent">
														<a onclick="clickSearchResult(this);" onmouseenter="hoverOnSearch(this);" onmouseleave="hoverOffSearch(this);"/>
													</div>
												</div>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <input type="text" id="managerPhoneInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="ManagerPhone"/>
                                                </xsl:attribute>
                                                </input>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <input type="text" id="managerEmailInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="ManagerEmail"/>
                                                </xsl:attribute>
                                                </input>							
                                            </td>
                                        </tr>
                                        <tr class="singlepropertyrow">
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                Person Contacted at Affected Program
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
												<img src="search icon.png" id="contactSearchIcon" style="float: right; width: 25px; height: 25px;" 
													onclick="displaySearchResults(this);"/>
                                                <input type="text" id="contactNameInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="ContactName"/>
                                                </xsl:attribute>
                                                </input>
												<div class="dropdown">
													<div id="contactDropdown" class="dropdowncontent">
														<a onclick="clickSearchResult(this);" onmouseenter="hoverOnSearch(this);" onmouseleave="hoverOffSearch(this);"/>
													</div>
												</div>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <input type="text" id="contactPhoneInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="ContactPhone"/>
                                                </xsl:attribute>
                                                </input>
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 25%">
                                                <input type="text" id="contactEmailInput" class="contactinput">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="ContactEmail"/>
                                                </xsl:attribute>
                                                </input>							
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table style="border: 1px solid black;">
                                    <tr>
                                        <td style="width: 50%; height: 100%"
											title="Select all DPHHS divisions that will/may be impacted by this decision request.">
                                            <table style="border: 1px solid black;">
                                                <tr>
                                                    <td class="propertylabel" style="width: 20%; vertical-align: text-top; padding-top: 5px;">
                                                        DPHHS Divisions Impacted
                                                    </td>
                                                    <xsl:for-each select="DivisionsImpacted">
                                                        <td style="width: 10%">
                                                            <ul style="list-style-type: none">
                                                                <li>
                                                                    <input type="checkbox" id="divimp1">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div1Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp1">AMDD</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp2">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div2Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp2">BFSD</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp3">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div3Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp3">CFSD</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp4">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div4Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp4">CSED</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp5">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div5Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp5">DETD</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp6">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div6Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp6">DO</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp7">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div7Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp7">DSD</label>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td style="width: 10%">
                                                            <ul>
                                                                <li>
                                                                    <input type="checkbox" id="divimp8">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div8Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp8">ECFSD</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp9">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div9Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp9">HCSD</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp10">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div10Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp10">HRD</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp11">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div11Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp11">PHSD</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp12">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div12Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp12">QAD</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp13">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div13Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp13">SLTC</label>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" id="divimp14">
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="Div14Name"/>
                                                                    </xsl:attribute>
                                                                    </input>
                                                                    <label for="divimp14">TSD</label>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    </xsl:for-each>
                                                    <td style="width: 60%"/>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 50%; height: 100%;"
											title="Select all DPHHS business processes that apply to the decision request.">
                                            <table style="border: 1px solid black;">
                                                <tr>
                                                    <td class="propertylabel" style="vertical-align: text-top; padding-top: 5px">
                                                        DPHHS Processes Involved
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            <li>
                                                                <input type="checkbox" id="armcb" onclick="processInvolvedClick(this);">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="ARMCheck"/>
                                                                </xsl:attribute>
                                                                </input>
                                                                <label for="armcb">ARM</label>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" id="spacb" onclick="processInvolvedClick(this);">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="SPACheck"/>
                                                                </xsl:attribute>
                                                                </input>
                                                                <label for="spacb">SPA</label>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" id="mcacb" onclick="processInvolvedClick(this);">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="MCACheck"/>
                                                                </xsl:attribute>
                                                                </input>
                                                                <label for="mcacb">MCA</label>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" id="waivercb" onclick="processInvolvedClick(this);">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="WaiverCheck"/>
                                                                </xsl:attribute>
                                                                </input>
                                                                <label for="waivercb">Waiver</label>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" id="procedurecb" onclick="processInvolvedClick(this);">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="ProcedureCheck"/>
                                                                </xsl:attribute>
                                                                </input>
                                                                <label for="procedurecb">Procedure</label>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" id="policycb" onclick="processInvolvedClick(this);">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="PolicyCheck"/>
                                                                </xsl:attribute>
                                                                </input>
                                                                <label for="policycb">Policy</label>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" id="othercb" onclick="processInvolvedClick(this);">
                                                                <xsl:attribute name="value">
                                                                    <xsl:value-of select="OtherCheck"/>
                                                                </xsl:attribute>
                                                                </input>
                                                                <label for="othercb">Other</label>
                                                            </li>
                                                        </ul>
                                                        <!-- Hidden selects to capture the checkbox inputs above into the form custom properties -->
                                                        <select style="display: none" id="armInvolved">
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="_321Z52B_0VTQHWGYV0000CJ_" />
                                                            </xsl:attribute>
                                                            <option value="321Z52B_0VTQHWGYV0000CH">
                                                                No
                                                            </option>
                                                            <option value="321Z52B_0VTQHWGYV0000CK">
                                                                Yes
                                                            </option>
                                                        </select>
                                                        <select style="display: none" id="spaInvolved">
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="_321Z52B_0VTQHDGYT000097_" />
                                                            </xsl:attribute>
                                                            <option value="321Z52B_0VTQHDGYT000096">
                                                                No
                                                            </option>
                                                            <option value="321Z52B_0VTQHDGYT000098">
                                                                Yes
                                                            </option>
                                                        </select>
                                                        <select style="display: none" id="mcaInvolved">
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="_321Z52B_0VTQHKGYV0001DP_" />
                                                            </xsl:attribute>
                                                            <option value="321Z52B_0VTQHKGYV0001DN">
                                                                No
                                                            </option>
                                                            <option value="321Z52B_0VTQHKGYV0001DQ">
                                                                Yes
                                                            </option>
                                                        </select>
                                                        <select style="display: none" id="waiverInvolved">
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="_321Z52B_0VTQHGGYV0000BJ_" />
                                                            </xsl:attribute>
                                                            <option value="321Z52B_0VTQHGGYV0000BH">
                                                                No
                                                            </option>
                                                            <option value="321Z52B_0VTQHGGYV0000BK">
                                                                Yes
                                                            </option>
                                                        </select>
                                                        <select style="display: none" id="procedureInvolved">
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="_321Z52B_0VTQHTGYV0000JG_" />
                                                            </xsl:attribute>
                                                            <option value="321Z52B_0VTQHTGYV0000JF">
                                                                No
                                                            </option>
                                                            <option value="321Z52B_0VTQHTGYV0000JH">
                                                                Yes
                                                            </option>
                                                        </select>
                                                        <select style="display: none" id="policyInvolved">
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="_321Z52B_0VTQHSGYV0001DM_" />
                                                            </xsl:attribute>
                                                            <option value="321Z52B_0VTQHSGYV0001DL">
                                                                No
                                                            </option>
                                                            <option value="321Z52B_0VTQHSGYV0001DN">
                                                                Yes
                                                            </option>
                                                        </select>
                                                        <select style="display: none" id="otherInvolved">
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="_321Z52B_0VTQHSGYV0001DQ_" />
                                                            </xsl:attribute>
                                                            <option value="321Z52B_0VTQHSGYV0001DP">
                                                                No
                                                            </option>
                                                            <option value="321Z52B_0VTQHSGYV0001DR">
                                                                Yes
                                                            </option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table style="border: 1px solid black;">
                                    <tbody>
                                        <tr class="singlepropertyrow"
											title="Select all areas of DPHHS business that the decision request will/may impact.">
                                            <td class="propertylabel" style="vertical-align: text-top; padding-top: 5px">
                                                DPHHS Business Impacts
                                            </td>
                                            <td class="propertycontent" style="WIDTH: 50%">
                                                <ul>
                                                    <li class="businessimpact">
                                                        <input type="checkbox" id="tribalcb" onclick="businessImpactClick(this);">
                                                        <xsl:attribute name="value">
                                                            <xsl:value-of select="TribalCheck"/>
                                                        </xsl:attribute>
                                                        </input>
                                                        <label for="tribalcb">Tribal Impact</label>
                                                    </li>
                                                    <li class="businessimpact">
                                                        <input type="checkbox" id="fiscalcb" onclick="businessImpactClick(this);">
                                                        <xsl:attribute name="value">
                                                            <xsl:value-of select="FiscalCheck"/>
                                                        </xsl:attribute>
                                                        </input>
                                                        <label for="fiscalcb">Fiscal Impact</label>
                                                    </li>
                                                    <li class="businessimpact">
                                                        <input type="checkbox" id="smallbusinesscb" onclick="businessImpactClick(this);">
                                                        <xsl:attribute name="value">
                                                            <xsl:value-of select="SmallBusinessCheck"/>
                                                        </xsl:attribute>
                                                        </input>
                                                        <label for="smallbusinesscb">Small Business Impact</label>
                                                    </li>
                                                    <li class="businessimpact">
                                                        <input type="checkbox" id="medicaidcb" onclick="businessImpactClick(this);">
                                                        <xsl:attribute name="value">
                                                            <xsl:value-of select="MedicaidCheck"/>
                                                        </xsl:attribute>
                                                        </input>
                                                        <label for="medicaidcb">Medicaid Impact</label>
                                                    </li>
                                                </ul>
                                                <!-- Hidden selects to capture the checkbox inputs above into the form custom properties -->
                                                <select style="display: none" id="tribalImpact">
                                                    <xsl:attribute name="value">
                                                        <xsl:value-of select="_321Z529_0VTQ6879W000005_" />
                                                    </xsl:attribute>
                                                    <option value="321Z529_0VTQ6879W000004">
                                                        No
                                                    </option>
                                                    <option value="321Z529_0VTQ6879W000006">
                                                        Yes
                                                    </option>
                                                </select>
                                                <select style="display: none" id="smallBusinessImpact">
                                                    <xsl:attribute name="value">
                                                        <xsl:value-of select="_321Z529_0VTQ6879W000008_" />
                                                    </xsl:attribute>
                                                    <option value="321Z529_0VTQ6879W000007">
                                                        No
                                                    </option>
                                                    <option value="321Z529_0VTQ6879W000009">
                                                        Yes
                                                    </option>
                                                </select>
                                                <select style="display: none" id="medicaidImpact">
                                                    <xsl:attribute name="value">
                                                        <xsl:value-of select="_321Z529_0VTQ6779W00000R_" />
                                                    </xsl:attribute>
                                                    <option value="321Z529_0VTQ6779W00000Q">
                                                        No
                                                    </option>
                                                    <option value="321Z529_0VTQ6779W00000S">
                                                        Yes
                                                    </option>
                                                </select>
                                                <select style="display: none" id="fiscalImpact">
                                                    <xsl:attribute name="value">
                                                        <xsl:value-of select="_321Z529_0VTQ6D79W000006_" />
                                                    </xsl:attribute>
                                                    <option value="321Z529_0VTQ6D79W000005">
                                                        No
                                                    </option>
                                                    <option value="321Z529_0VTQ6D79W000007">
                                                        Yes
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br/>
                                <strong>DPHHS Strategic Plan Alignment</strong>
                                <table id="planTable" style="border: 1px solid black;">
                                    <tbody>
                                        <xsl:for-each select="DeptMissions/DeptMission">
                                            <tr repeat="orderRow">
                                                <td style="text-align:right;">
                                                    <input type="button" value="Delete Row" onclick="deletePlanRow(this);"/>
                                                    <table style="border: none;">
                                                        <tr id="goalRow">
                                                            <td>
                                                                <table style="border: 1px solid black">
                                                                    <tbody>
                                                                        <tr class="singlepropertyrow">
                                                                            <td class="propertylabel">DPHHS Strategic Goal</td>
                                                                            <td class="propertycontent" style="WIDTH: 100%"
																				title="Select the DPHHS strategic goal that this decision is associated with.">
                                                                                <select id="goalSelect" class="js-goal-select" style="WIDTH: 100%" onchange="setObjectiveOptions(this);">
                                                                                    <xsl:attribute name="value">
                                                                                        <xsl:value-of select="GoalSelection" />
                                                                                    </xsl:attribute>
                                                                                    <xsl:for-each select="document('DBrief_Lookups.xml')/lookups/Goals/Goal">
                                                                                        <option>
                                                                                            <xsl:value-of select="value"/>
                                                                                        </option>
                                                                                    </xsl:for-each>
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="objectiveRow">
                                                            <td>
                                                                <table style="border: 1px solid black">
                                                                    <tbody>
                                                                        <tr class="singlepropertyrow">
                                                                            <td class="propertylabel">DPHHS Strategic Objective</td>
                                                                            <td id="objectiveSelectParent" class="propertycontent" style="WIDTH: 100%"
																				title="Select the DPHHS strategic objective that this decision is associated with.">
                                                                                <select id="objectiveSelect" class="js-objective-select" style="WIDTH: 100%" onchange="setObjective(this);">
                                                                                    <xsl:attribute name="value">
                                                                                        <xsl:value-of select="ObjectiveSelection" />
                                                                                    </xsl:attribute>
                                                                                    <xsl:for-each select="document('DBrief_Lookups.xml')/lookups/Objectives/Objective">
                                                                                        <option>
                                                                                            <xsl:value-of select="value"/>
                                                                                        </option>
                                                                                    </xsl:for-each>
                                                                                </select>
                                                                                <input type="text" class="js-objective-index" style="display: none">
                                                                                <xsl:attribute name="value">
                                                                                    <xsl:value-of select="ObjectiveIndex" />
                                                                                </xsl:attribute>
                                                                                </input>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="strategyRow">
                                                            <td>
                                                                <table style="border: 1px solid black">
                                                                    <tbody>
                                                                        <tr class="singlepropertyrow">
                                                                            <td class="propertylabel">DPHHS Strategy</td>
                                                                            <td id="strategySelectParent" class="propertycontent" style="WIDTH: 100%"
																				title="Select the DPHHS strategy that this decision is associated with.">
                                                                                <select id="strategySelect" class="js-strategy-select" style="WIDTH: 100%" onchange="setStrategyIndex(this);">
                                                                                    <xsl:attribute name="value">
                                                                                        <xsl:value-of select="StrategySelection" />
                                                                                    </xsl:attribute>
                                                                                    <xsl:for-each select="document('DBrief_Lookups.xml')/lookups/Strategies/Strategy">
                                                                                        <option>
                                                                                            <xsl:value-of select="value"/>
                                                                                        </option>
                                                                                    </xsl:for-each>
                                                                                </select>
                                                                                <input type="text" class="js-strategy-index" style="display: none">
                                                                                <xsl:attribute name="value">
                                                                                    <xsl:value-of select="StrategyIndex" />
                                                                                </xsl:attribute>
                                                                                </input>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td style="text-align:right;">	
                                                <input type="button" class="NormalButton" repeat="orderRow" value="Add Row" />																
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <table style="border: 1px solid black;">
                                    <tr>
                                        <td class="propertylabel">Branch Strategic Goal</td>
                                        <td>
                                            <input type="text" style="width:100%"
												title="Provide any additional branch strategic goal that this decision request may be related to.">
                                            <xsl:attribute name="value">
                                                <xsl:value-of select="BranchStrategicGoal"/>
                                            </xsl:attribute>
                                            </input>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="propertylabel">Division Strategic Goal</td>
                                        <td>
                                            <input type="text" style="width:100%"
												title="Provide any additional division strategic goal that this decision request may be related to.">
                                            <xsl:attribute name="value">
                                                <xsl:value-of select="DivisionStrategicGoal"/>
                                            </xsl:attribute>
                                            </input>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="propertylabel">Other Strategic Alignment</td>
                                        <td>
                                            <input type="text" style="width:100%"
												title="Provide any other strategic alignment that this decision request may be related to.">
                                            <xsl:attribute name="value">
                                                <xsl:value-of select="OtherStrategicAlignment"/>
                                            </xsl:attribute>
                                            </input>
                                        </td>
                                    </tr>
                                </table>
                                <table style="border: 1px solid black; width: 50%">
                                    <tbody>
                                        <tr class="singlepropertyrow">
                                            <td class="propertylabel">Implementation Date</td>
                                            <td class="propertycontent" style="WIDTH: 50%">
                                                <input class="propertytextinput" style="WIDTH: 100%"
													title="Provide the date for when this decision request will take effect.">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="_321Z525_0VTPFYHRB00027V_" />
                                                </xsl:attribute>
                                                </input>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br/>
                                <strong>Issue Definition</strong>
                                <p>
                                    <textarea class="textareainput" rows="1" cols="1"
										title="Provide a high-level statement about the issue that is trying to be solved by this decision request.">
										<xsl:value-of select="IssueDefinition"/>
									</textarea>
                                    &#160;
                                </p>
                                <strong>Executive Summary</strong>
                                <p>
                                    <textarea class="textareainput" rows="1" cols="1"
										title="Provide more detailed information to the decision maker about the current situation of the issue.">
										<xsl:value-of select="ExecutiveSummary"/>
									</textarea>
                                    &#160;
                                </p>
                                <strong>Identification of Alternatives</strong>
                                <p class="optiontitle">Option 1:</p>
                                <p>
                                    <textarea class="textareainput" rows="1" cols="1"
										title="Provide the first option the decision maker of this decision request could consider as a possible solution.">
										<xsl:value-of select="IdentificationOfAlternatives/Option1" />
									</textarea>
                                    &#160;
                                </p>
                                <p class="optiontitle">Option 2:</p>
                                <p>
                                    <textarea class="textareainput" rows="1" cols="1"
										title="Provide the second option the decision maker of this decision request could consider as a possible solution.">
										<xsl:value-of select="IdentificationOfAlternatives/Option2" />
									</textarea>
                                    &#160;
                                </p>
                                <p class="optiontitle">Option 3:</p>
                                <p>
                                    <textarea class="textareainput" rows="1" cols="1"
										title="Provide the third option the decision maker of this decision request could consider as a possible solution.">
										<xsl:value-of select="IdentificationOfAlternatives/Option3" />
									</textarea>
                                    &#160;
                                </p>
                                <strong>Comparison of Alternatives</strong>
                                <p class="optiontitle">Option 1:</p>
                                <p>
                                    <textarea class="textareainput" rows="1" cols="1"
										title="Provide the benefits and risks of the first option for the decision maker's consideration.">
										<xsl:value-of select="ComparisonOfAlternatives/Option1" />
									</textarea>
                                    &#160;
                                </p>
                                <p class="optiontitle">Option 2:</p>
                                <p>
                                    <textarea class="textareainput" rows="1" cols="1"
										title="Provide the benefits and risks of the second option for the decision maker's consideration.">
										<xsl:value-of select="ComparisonOfAlternatives/Option2" />
									</textarea>
                                    &#160;
                                </p>
                                <p class="optiontitle">Option 3:</p>
                                <p>
                                    <textarea class="textareainput" rows="1" cols="1"
										title="Provide the benefits and risks of the third option for the decision maker's consideration.">
										<xsl:value-of select="ComparisonOfAlternatives/Option3" />
									</textarea>
                                    &#160;
                                </p>
                                <strong>Recommendation</strong>
                                <p>
                                    <textarea class="textareainput" rows="1" cols="1"
										title="Provide and describe the option that the submitter of this decision request recommends as the best solution to the issue.">
										<xsl:value-of select="Recommendation"/>
									</textarea>
                                    &#160;
                                </p>
                                <!-- <strong>Who is the request being sent to?</strong>
                                <table style="width: 50%">
                                    <tr class="routeTypeRow">
                                        <td class="propertylabel">Route Type</td>
                                        <td>
                                            <select class="submitRouteType" id="submitRouteType" onchange="getSupervisorName(this);" style="width: 100%">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="RouteType"/>
                                                </xsl:attribute>
                                                <option/>
                                                <option>Automatic Route to Supervisor</option>
                                                <option>Manual Route</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="routeToRow">
                                        <td class="propertylabel">Route To:</td>
                                        <td>
                                            <input class="submitRouteTo" id="submitRouteTo" type="text" style="width: 100%">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="RouteTo"/>
                                                </xsl:attribute>
                                            </input>
                                        </td>
                                    </tr>
                                </table> -->
                                <div id="reviewerComments" style="display: none">
                                    <input type="checkbox" id="showCommentsCB" style="display: none">
                                        <xsl:attribute name="value">
                                            <xsl:value-of select="ShowComments"/>
                                        </xsl:attribute>
                                    </input>
                                    <br/>
                                    <strong>Reviewer Comments</strong>
                                    <br/>
                                    <input id="addComment" type="button" value="Add Comment" style="display: none" onclick="addFirstComment();"/>
                                    <table id="commentTable" style="border: 1px solid black;">
                                        <tbody>
                                            <xsl:for-each select="Comments/Comment">
                                                <tr repeat="orderRow">
                                                    <td style="text-align:right">
                                                        <input type="button" value="Delete Comment" onclick="deleteCommentRow(this);"/>
                                                        <table style="border: none; width: 960px;">
                                                            <tr>
                                                                <td colspan="1" style="width: 50%">
                                                                    <table style="border: 1px solid black">
                                                                        <tr class="singlepropertyrow">
                                                                            <td class="propertylabel">Reviewer Name</td>
                                                                            <td class="propertycontent" style="WIDTH: 50%">
                                                                                <input id="reviewerName" class="js-reviewer-name" type="text" style="WIDTH: 100%" readonly="readonly"
                                                                                    title="The name of the reviewer leaving this comment.">	
                                                                                <xsl:attribute name="value">
                                                                                    <xsl:value-of select="ReviewerName"/>
                                                                                </xsl:attribute>
                                                                                </input>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td colspan="1" style="width: 50%">
                                                                    <table style="border: 1px solid black">
                                                                        <tr>
                                                                            <td class="propertylabel">Comment Date</td>
                                                                            <td class="propertycontent" style="WIDTH: 50%">
                                                                                <input id="commentTimestamp" class="js-comment-timestamp" type="text" style="WIDTH: 100%" readonly="readonly"
                                                                                    title="The date and time of this comment. Generated automatically.">	
                                                                                <xsl:attribute name="value">
                                                                                    <xsl:value-of select="Timestamp"/>
                                                                                </xsl:attribute>
                                                                                </input>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table style="border: 1px solid black;">
                                                                        <tr>
                                                                            <td class="propertylabel">Comments</td>
                                                                            <td>
                                                                                <p>
                                                                                    <textarea id="reviewerComment" class="js-reviewer-comment" rows="1" cols="1" readonly="readonly"
                                                                                        title="Provide additional comments on the decision request.">
                                                                                        <xsl:value-of select="Comment"/>
                                                                                    </textarea>
                                                                                    &#160;
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table style="border: 1px solid black;">
                                                                        <tr>
                                                                            <td class="propertylabel">Recommendation/Decision</td>
                                                                            <td>
                                                                                <p>
                                                                                    <textarea id="recommendationDecision" class="js-recommendation-decision" rows="1" cols="1" readonly="readonly"
                                                                                        title="Provide a decision if the decision request does not need additional approval or provide a recommendation if additional approval is required.">
                                                                                        <xsl:value-of select="RecommendationDecision"/>
                                                                                    </textarea>
                                                                                    &#160;
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="1" style="width: 480px">
                                                                    <table style="border: 1px solid black; width: 480px;">
                                                                        <tr>
                                                                            <td class="propertylabel">Reviewer Action</td>
                                                                            <td class="propertycontent" 
                                                                                title="Select an action to perform with this decision request. Hover over options for more information.">
                                                                                <select id="actionSelect" class="js-action-select" disabled="disabled" onchange="setProcessStatus(this);" style="width: 480px" title="Select what action to take on this decision request. Hover over selections for more information.">
                                                                                    <xsl:attribute name="value">
                                                                                        <xsl:value-of select="Action"/>
                                                                                    </xsl:attribute>
                                                                                    <option/>
                                                                                    <option title="Send request to approval queue for archiving and on to downstream workflows.">Approve</option>
                                                                                    <option title="Send request to somone else in the department for additional review.">Request Further Review</option>
                                                                                    <option title="Send request back to requester for revisions.">Request Revisions</option>
                                                                                    <option title="Send request to denial queue for deletion.">Deny</option>
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="js-route-type-row" style="display: none; width: 100%;">
                                                                            <td class="propertylabel">Route Type</td>
                                                                            <td title="Choose to automatically route to supervisor or manual route to someone else in the department." style="width: 100%;">
                                                                                <select class="js-route-type" onchange="getSupervisorName(this);" disabled="disabled" style="width: 480px;">
                                                                                    <xsl:attribute name="value">
                                                                                        <xsl:value-of select="RouteType"/>
                                                                                    </xsl:attribute>
                                                                                    <option/>
                                                                                    <option>Automatic Route to Supervisor</option>
                                                                                    <option>Manual Route</option>
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="js-route-to-row" style="display: none; width: 100%;">
                                                                            <td class="propertylabel">Route To:</td>
                                                                            <td style="width: 480px%;">
                                                                                <img src="search icon.png" id="routeSearchIcon" style="float: right; width: 25px; height: 25px;" 
                                                                                    onclick="displaySearchResults(this);"/>
                                                                                <input class="js-route-to" type="text" readonly="readonly" style="width: 480px;"
                                                                                    title="Who the decision request will be routed to next.">
                                                                                <xsl:attribute name="value">
                                                                                    <xsl:value-of select="RouteTo"/>
                                                                                </xsl:attribute>
                                                                                </input>
                                                                                <div class="dropdown">
                                                                                    <div id="routeDropdown" class="dropdowncontent">
                                                                                        <a onclick="clickSearchResult(this);" onmouseenter="hoverOnSearch(this);" onmouseleave="hoverOffSearch(this);"/>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td style="text-align:right;">	
                                                    <input type="button" class="NormalButton" repeat="orderRow" value="Add Comment" onclick="addCommentClick();"/>																
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <input type="button" id="routeButton" value="SUBMIT" style="display: none; padding: 10px" onclick="routeRequest();"/>			
                                <br/>
                                <br/>
								<!-- Hidden element for everyone who has touched this form -->					
								<table style="display: none;">
								  <thead>
									<tr>
									  <td class="sectionheader" colspan="2">DPHHS - DBrief
									  All Reviewers</td>
									</tr>
									<tr class="multipropertyheaderrow">
									  <td class="propertyheader"></td>
									  <td class="propertyheader" style="WIDTH: 100%">DPHHS
									  - DBrief Reviewers</td>
									</tr>
								  </thead>
								  <tbody>
									<xsl:for-each select="_321Z525_0VTPFDHRB00085N_S_/_321Z525_0VTPFDHRB00085N_">
									  <tr class="multipropertyrow"
									  repeat="321Z525_0VTPFDHRB00085N" deleterow="infd"
									  delete="321Z525_0VTPFDHRB00085N">
										<xsl:variable name="rowPos" select="position()" />
										<xsl:attribute name="deleterow">
										  <xsl:value-of select="concat('321Z525_0VTPFDHRB00085N_', $rowPos)" />
										</xsl:attribute>
										<td class="propertycontent">
										  <input type="button" value="X" deleterow="infd">
											<xsl:attribute name="deleterow">
											  <xsl:value-of select="concat('321Z525_0VTPFDHRB00085N_', $rowPos)" />
											</xsl:attribute>
										  </input>
										</td>
										<td class="propertycontent">
										  <input class="js-hdn-reviewer-name"
										  style="WIDTH: 100%">
											<xsl:attribute name="value">
											  <xsl:value-of select="_321Z525_0VTPFXHRB0002MC_" />
											</xsl:attribute>
										  </input>
										</td>
									  </tr>
									</xsl:for-each>
								  </tbody>
								  <tfoot>
									<tr class="multipropertyfooterrow"
									repeat="321Z525_0VTPFDHRB00085N">
									  <td class="propertycontent">
										<input type="button" value="+" id="reviewerButton"
										repeat="321Z525_0VTPFDHRB00085N"></input>
									  </td>
									</tr>
								  </tfoot>
								</table>
                                <!-- Hidden elements for iScript -->
                                <input type="button" id="btnGetGoals" value="" style="display:none" dbCall_onclick="getGoals" dbCall_param="1"/>
                                <input type="hidden" id="hdnGetGoals" dbSet="getGoals" dbSet_param="1"/>
								<!-- Hidden elements for javascript -->
								<input id="hdnRequesterID" style="display: none;">
									<xsl:attribute name="value">
										<xsl:value-of select="_321Z525_0VTPG2HRB00025P_" />
									</xsl:attribute>
								</input>
								<input id="hdnRequesterEmail" style="display: none;">
									<xsl:attribute name="value">
										<xsl:value-of select="_321Z525_0VTPFEHRB00080E_" />
									</xsl:attribute>
								</input>
								<input id="hdnAssigneeID" style="display: none;">
									<xsl:attribute name="value">
										<xsl:value-of select="_321Z525_0VTPG2HRB00025N_" />
									</xsl:attribute>
								</input>
								<input id="hdnAssigneeEmail" style="display: none;">
									<xsl:attribute name="value">
										<xsl:value-of select="_321Z525_0VTPFPHRB0002EV_" />
									</xsl:attribute>
								</input>
								<input id="hdnRoutingID" style="display: none;">
									<xsl:attribute name="value">
										<xsl:value-of select="_321Z525_0VTPFDHRB00085H_" />
									</xsl:attribute>
								</input>
								<input id="hdnRoutingEmail" style="display: none;">
									<xsl:attribute name="value">
										<xsl:value-of select="_321Z525_0VTPFPHRB0002EW_" />
									</xsl:attribute>
								</input>
                            </td>
                        </tr>
                    </tbody>
                </table>

                
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>