<?xml version="1.0" encoding="utf-8"?>
<lookups>
	<Goals>
		<Goal>
			<value></value>
		</Goal>
		<Goal>
			<value>1. Promote health equity and improve population health</value>
		</Goal>
		<Goal>
			<value>2. Strengthen the economic and social well-being of Montanans across the lifespan</value>
		</Goal>
		<Goal>
			<value>3. Ensure all children and youth achieve their highest potential</value>
		</Goal>
		<Goal>
			<value>4. Effectively engage stakeholders</value>
		</Goal>
		<Goal>
			<value>5. Ensure core business services are efficient, innovative, and transparent</value>
		</Goal>
		<Goal>
			<value>6. Improve organizational health and be the employer of choice</value>
		</Goal>
	</Goals>
	<Objectives>
		<Objective>
			<value></value>
		</Objective>
		<Objective>
			<value>1.1 Decrease health disparities</value>
		</Objective>
		<Objective>
			<value>1.2 Increase access to timely, affordable, and effective health services</value>
		</Objective>
		<Objective>
			<value>1.3 Strengthen prevention efforts to promote health and well-being</value>
		</Objective>
		<Objective>
			<value>1.4 Improve the public health system capacity</value>
		</Objective>
		<Objective>
			<value>2.1 Encourage individual and family stability</value>
		</Objective>
		<Objective>
			<value>2.2 Support individuals’ ability to work and be self-sufficient</value>
		</Objective>
		<Objective>
			<value>2.3 Maximize the opportunity for independence, well-being, and health among older adults, 
				       people with disabilities, and their families and caregivers</value>
		</Objective>
		<Objective>
			<value>3.1 Improve birth outcomes for mothers and babies</value>
		</Objective>
		<Objective>
			<value>3.2 Support the developmental needs of children and youth</value>
		</Objective>
		<Objective>
			<value>3.3 Increase access to high quality care and education</value>
		</Objective>
		<Objective>
			<value>3.4 Promote safe, stable, and nurturing relationships and environments</value>
		</Objective>
		<Objective>
			<value>4.1 Enhance and expand engagement with clients</value>
		</Objective>
		<Objective>
			<value>4.2 Enhance collaboration with tribal agencies and organizations serving American Indians</value>
		</Objective>
		<Objective>
			<value>4.3 Engage additional stakeholders essential to program and service delivery</value>
		</Objective>
		<Objective>
			<value>5.1 Increase use of effective planning, evaluation, and management principles across the Department</value>
		</Objective>
		<Objective>
			<value>5.2 Strengthen coordination and collaboration across branches, divisions, and programs</value>
		</Objective>
		<Objective>
			<value>5.3 Enhance use of financial resources</value>
		</Objective>
		<Objective>
			<value>5.4 Optimize information technology investments to improve process efficiency and enable innovation</value>
		</Objective>
		<Objective>
			<value>6.1 Build and sustain the organizational infrastructure and workforce to advance equity, diversity, and inclusion</value>
		</Objective>
		<Objective>
			<value>6.2 Support skill building, advancement, and mobility in the workforce</value>
		</Objective>
		<Objective>
			<value>6.3 Support work-life balance, workplace health and wellness, and safety in Department activities and policies</value>
		</Objective>
	</Objectives>
	<Strategies>
		<Strategy>
			<value></value>
		</Strategy>
		<Strategy>
			<value>1.1.1 Increase the use of population and program data to inform decision-making</value>
		</Strategy>
		<Strategy>
			<value>1.1.2 Collaborate with partners to effectively address social determinants of health and sources of health disparities</value>
		</Strategy>
		<Strategy>
			<value>1.1.3 Review policy changes for health equity impacts</value>
		</Strategy>
		<Strategy>
			<value>1.1.4 Support access to culturally appropriate, patient-centered health services, 
			             particularly by underserved and at-risk populations</value>
		</Strategy>
		<Strategy>
			<value>1.1.5 Enhance effective care coordination and/or case management across programs consistent with client needs and preferences</value>
		</Strategy>
		<Strategy>
			<value>1.2.1 Develop, support, and implement policies to increase access to affordable and effective health services</value>
		</Strategy>
		<Strategy>
			<value>1.2.2 Assess and implement options for addressing health care provider shortages statewide</value>
		</Strategy>
		<Strategy>
			<value>1.2.3 Support continued expansion of telehealth services for behavioral health, primary care, and other health-related needs</value>
		</Strategy>
		<Strategy>
			<value>1.2.4 Promote the availability of cost-effective services and supports meeting the continuum of health needs</value>
		</Strategy>
		<Strategy>
			<value>1.2.5 Support the implementation of evidence-based interventions in collaboration with health care systems, 
				         providers, and other partners</value>
		</Strategy>
		<Strategy>
			<value>1.2.6 Continue to ensure programs and services meet Americans with Disabilities Act (ADA) requirements</value>
		</Strategy>
		<Strategy>
			<value>1.3.1 Develop, implement, incentivize, and monitor effective policies and programs that promote 
			             health through prevention, wellness, and early intervention</value>
		</Strategy>
		<Strategy>
			<value>1.3.2 Increase health literacy and provide information to help Montanans make healthy choices</value>
		</Strategy>
		<Strategy>
			<value>1.3.3 Create environments that support healthy choices and lifestyles</value>
		</Strategy>
		<Strategy>
			<value>1.3.4 Use effective strategies to decrease stigma about behavioral health</value>
		</Strategy>
		<Strategy>
			<value>1.3.5 Increase immunization and health screening rates</value>
		</Strategy>
		<Strategy>
			<value>1.4.1 Conduct epidemiological investigations in collaboration with partners to identify public health 
			             problems and implement public health actions</value>
		</Strategy>
		<Strategy>
			<value>1.4.2 Provide technical assistance to tribal and local health departments and other partners for 
			             data analysis and epidemiologic support </value>
		</Strategy>
		<Strategy>
			<value>1.4.3 Support the development of community health assessments and community health improvement plans at tribal and local levels</value>
		</Strategy>
		<Strategy>
			<value>1.4.4 Build capacity to monitor and respond to environmental health concerns in Montana communities</value>
		</Strategy>
		<Strategy>
			<value>1.4.5 Prepare public health and health care systems at the state, tribal, and local levels to plan for and 
			             respond to significant public health events and emergencies</value>
		</Strategy>
		<Strategy>
			<value>2.1.1 Effectively connect individuals and families to needed services and supports related to food, housing/shelter, 
			             safety, transportation, health, behavioral health, child care and education, youth engagement, and social supports, 
						 including through increased client advocacy and program coordination</value>
		</Strategy>
		<Strategy>
			<value>2.1.2 Enhance collaboration and coordination to facilitate access to effective preventative supports and safety interventions 
			             for at-risk families</value>
		</Strategy>
		<Strategy>
			<value>2.1.3 Enhance the lives of children and families by helping parents meet the financial and medical needs of their children by 
			             establishing and enforcing child support orders</value>
		</Strategy>
		<Strategy>
			<value>2.2.1 Build on client strengths and address barriers through assessment, goal-setting, client advocacy, benefits counseling, and 
						 services and supports coordination</value>
		</Strategy>
		<Strategy>
			<value>2.2.2 Support clients to gain the skills, education, and experience needed to find and maintain employment through completion of 
					     education, training programs, and work experience</value>
		</Strategy>
		<Strategy>
			<value>2.2.3 Support individual and family financial security through increased employment and earnings</value>
		</Strategy>
		<Strategy>
			<value>2.3.1 Promote person-centered, informed-choice, and selfdetermination for individual-driven service plans, including through 
			             building self-advocacy skills</value>
		</Strategy>
		<Strategy>
			<value>2.3.2 Continue to expand conflict free assessment to eliminate conflict of interest</value>
		</Strategy>
		<Strategy>
			<value>2.3.3 Increase capacity of natural and informal supports through education, outreach, respite, and other support services</value>
		</Strategy>
		<Strategy>
			<value>2.3.4 Support clients to live in home and community-based settings</value>
		</Strategy>
		<Strategy>
			<value>2.3.5 Increase business engagement to promote the employment of people with disabilities and older Montanans and the ongoing 
			             implementation of Workforce Innovation and Opportunity Act (WIOA)</value>
		</Strategy>
		<Strategy>
			<value>2.3.6 Provide education and training to keep older Montanans safe from abuse, neglect, and exploitation</value>
		</Strategy>
		<Strategy>
			<value>3.1.1 Expand coordinated prenatal and postpartum care, education, and resources to support positive physical and 
			             behavioral health outcomes for mothers and babies</value>
		</Strategy>
		<Strategy>
			<value>3.1.2 Expand access to evidence-based family supports and services for Montana families</value>
		</Strategy>
		<Strategy>
			<value>3.1.3 Reduce teen and unintended pregnancies</value>
		</Strategy>
		<Strategy>
			<value>3.2.1 Provide training and technical assistance to primary care providers, early childhood care and 
			             education providers, and other community partners who serve children to increase use of 
						 developmental screenings, referrals, and connections to services</value>
		</Strategy>
		<Strategy>
			<value>3.2.2 Support primary care providers and care teams to increase their capacity to address behavioral health concerns in 
			             children and youth</value>
		</Strategy>
		<Strategy>
			<value>3.2.3 Promote emotional resilience in children and youth through traumainformed wellness and prevention efforts</value>
		</Strategy>
		<Strategy>
			<value>3.2.4 Increase access to transition services for youth with disabilities, youth in care, and underserved populations</value>
		</Strategy>
		<Strategy>
			<value>3.2.5 Improve transitions between services and programs for children and youth, including the transition to adulthood</value>
		</Strategy>
		<Strategy>
			<value>3.3.1 Continue to expand access to a mixed delivery system of high-quality early childhood care and education, with a focus on 
			             underserved and at-risk populations</value>
		</Strategy>
		<Strategy>
			<value>3.3.2 Increase access to skill-building and training opportunities for school-age youth, including high quality out of 
			             school time care and opportunities for youth with disabilities</value>
		</Strategy>
		<Strategy>
			<value>3.3.3 Support care and education (early childhood care and education and K-12) professionals through best practices sharing, 
			             professional development, coaching, mentoring, and pay equity</value>
		</Strategy>
		<Strategy>
			<value>3.3.4 Improve referral networks for early childhood care and education</value>
		</Strategy>
		<Strategy>
			<value>3.3.5 Improve children’s school readiness and successful transitions to kindergarten</value>
		</Strategy>
		<Strategy>
			<value>3.3.6 Increase parent education, choice, engagement, and partnership in early childhood care and education</value>
		</Strategy>
		<Strategy>
			<value>3.4.1 Increase access to appropriate evidence-based, informed, and culturally competent programs to support and 
			             strengthen families and communities</value>
		</Strategy>
		<Strategy>
			<value>3.4.2 Increase public knowledge of and capacity to address adverse childhood experiences and other multigenerational, complex trauma</value>
		</Strategy>
		<Strategy>
			<value>3.4.3 Build capacity for community-based efforts to increase protective factors, reduce risk factors, and prevent maltreatment</value>
		</Strategy>
		<Strategy>
			<value>3.4.4 Provide resources and support to develop and enhance traumainformed organizations and care</value>
		</Strategy>
		<Strategy>
			<value>3.4.5 Support child protective service providers and the legal community to increase safety, wellbeing, and permanency</value>
		</Strategy>
		<Strategy>
			<value>4.1.1 Regularly solicit input from clients and integrate feedback into efforts to enhance service delivery and client satisfaction</value>
		</Strategy>
		<Strategy>
			<value>4.1.2 Better use technology to support information sharing and client engagement</value>
		</Strategy>
		<Strategy>
			<value>4.1.3 Provide centralized information and resources needed by individuals and families</value>
		</Strategy>
		<Strategy>
			<value>4.1.4 Improve access to services through no-wrong-door practices and an online portal for Montanans</value>
		</Strategy>
		<Strategy>
			<value>4.1.5 Engage people with lived experience as partners and leaders in health and human services programs and systems</value>
		</Strategy>
		<Strategy>
			<value>4.2.1 Collaborate with tribal governments, tribal agencies, and urban Indian populations, honoring principles of: 
			             commitment to cooperation and collaboration; mutual understanding and respect; regular and early communication; 
						 a process of accountability for addressing issues; and preservation of the tribal-state relationship</value>
		</Strategy>
		<Strategy>
			<value>4.2.2 Adopt policies and practices that acknowledge and support the sovereignty of tribal governments</value>
		</Strategy>
		<Strategy>
			<value>4.2.3 Increase access to Department programs through education, information sharing, and direct communication</value>
		</Strategy>
		<Strategy>
			<value>4.2.4 Work collaboratively to address health and human services issues that cross jurisdictional boundaries</value>
		</Strategy>
		<Strategy>
			<value>4.2.5 Increase the cultural competency of Department staff, providers, and contractors to gain understanding of tribal governments, 
			             protocols, and practices and consider the impact of decisionmaking on tribal partners and communities </value>
		</Strategy>
		<Strategy>
			<value>4.2.6 Consistently communicate with tribal governments about policy and program changes through appropriate means and with 
			             adequate time to review and provide input</value>
		</Strategy>
		<Strategy>
			<value>4.2.7 Support bilateral capacity building and sharing of expertise to improve the health and well-being of all Montanans</value>
		</Strategy>
		<Strategy>
			<value>4.3.1 Regularly solicit input from partners and integrate feedback into efforts to enhance service delivery and client satisfaction</value>
		</Strategy>
		<Strategy>
			<value>4.3.2 Engage providers, advocates, and interested parties to understand and react to strengths and needs</value>
		</Strategy>
		<Strategy>
			<value>4.3.3 Provide clear policy and procedure guidance to contractors and providers</value>
		</Strategy>
		<Strategy>
			<value>4.3.4 Improve provider and contractor education and support through mentoring, technical assistance, sharing best practices, 
			             education/training supports, and other professional development opportunities</value>
		</Strategy>
		<Strategy>
			<value>4.3.5 Effectively engage stakeholders on complex issues to further public understanding of the Department’s goals and actions</value>
		</Strategy>
		<Strategy>
			<value>4.3.6 Build capacity to implement best practices and innovative policy approaches to strengthen Department programs and services</value>
		</Strategy>
		<Strategy>
			<value>5.1.1 Evaluate and report on the effectiveness of programs and initiatives</value>
		</Strategy>
		<Strategy>
			<value>5.1.2 Increase the use of operational data, performance management systems, and continuous quality improvement processes to 
			             identify, define, monitor, and share progress on the performance of strategic initiatives</value>
		</Strategy>
		<Strategy>
			<value>5.1.3 Integrate data systems and modernize capacity for data analytics through enterprise data solutions and software</value>
		</Strategy>
		<Strategy>
			<value>5.1.4 Support consistent data governance policies and processes</value>
		</Strategy>
		<Strategy>
			<value>5.1.5 Integrate project management concepts and principles as a business solution</value>
		</Strategy>
		<Strategy>
			<value>5.1.6 Develop, update, and maintain documentation for internal controls, business processes, and decision-making</value>
		</Strategy>
		<Strategy>
			<value>5.2.1 Improve communication and coordination across branches, divisions, and programs to better deliver services to clients</value>
		</Strategy>
		<Strategy>
			<value>5.2.2 Refine processes to support improved client transitions between programs and services</value>
		</Strategy>
		<Strategy>
			<value>5.2.3 Standardize and coordinate processes across the Department for increased efficiency and improved quality 
			             (e.g. policy making, utilization review, state plans, etc.)</value>
		</Strategy>
		<Strategy>
			<value>5.3.1 Increase financial, procurement, budget and contract training to enhance capacity for financial management</value>
		</Strategy>
		<Strategy>
			<value>5.3.2 Maximize new funding sources to support increased and improved services</value>
		</Strategy>
		<Strategy>
			<value>5.3.3 Ensure budgetary stability and responsible resource utilization, including blending funding across programs and 
			             divisions to improve service delivery</value>
		</Strategy>
		<Strategy>
			<value>5.3.4 Continue to pursue public-private partnerships to maximize impact, support innovation, and enhance sustainability</value>
		</Strategy>
		<Strategy>
			<value>5.4.1 Use a governance framework to ensure information technology investments advance Department mission and goals</value>
		</Strategy>
		<Strategy>
			<value>5.4.2 Efficiently deliver payments electronically</value>
		</Strategy>
		<Strategy>
			<value>5.4.3 Continue development of enterprise systems with common client identifiers</value>
		</Strategy>
		<Strategy>
			<value>5.4.4 Increase end user capacity knowledge and skills with business solution tools</value>
		</Strategy>
		<Strategy>
			<value>5.4.5 Protect the safety and integrity of the Department’s physical and digital assets</value>
		</Strategy>
		<Strategy>
			<value>6.1.1 Improve and enhance staff recruitment and retention practices</value>
		</Strategy>
		<Strategy>
			<value>6.1.2 Analyze pay structure across the Department, and work to achieve equity and competitiveness</value>
		</Strategy>
		<Strategy>
			<value>6.1.3 Strive to ensure a diverse workforce, reflecting a range of life experiences and perspectives</value>
		</Strategy>
		<Strategy>
			<value>6.1.4 Implement succession planning strategies</value>
		</Strategy>
		<Strategy>
			<value>6.1.5 Continue to study staffing capacity to inform human resources decision-making</value>
		</Strategy>
		<Strategy>
			<value>6.2.1 Develop multi-track career ladders and associated performance management approaches to support the professional goals of staff</value>
		</Strategy>
		<Strategy>
			<value>6.2.2 Engage staff in identifying and accessing training, continuing education, and leadership opportunities for skill development</value>
		</Strategy>
		<Strategy>
			<value>6.2.3 Regularly engage employees to assess and continuously improve workplace culture</value>
		</Strategy>
		<Strategy>
			<value>6.2.4 Provide consistent opportunities and supports for employees throughout the state</value>
		</Strategy>
		<Strategy>
			<value>6.3.1 Work with the Department of Administration, Human Resources Division to promote employee work-life balance to prevent burnout</value>
		</Strategy>
		<Strategy>
			<value>6.3.2 Support wellness and safety programming for employees</value>
		</Strategy>
		<Strategy>
			<value>6.3.3 Foster a culture of value and inclusion</value>
		</Strategy>
	</Strategies>
</lookups>